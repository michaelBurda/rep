<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Reports';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?=Yii::t('app', 'Welcome to Reports Project')?></h1>

        <?php if (Yii::$app->user->isGuest) { ?>

            <p class="lead"><?=Yii::t('app', 'To get started, you must authorize')?></p>

            <p><a class="nbtn nbtn-lg nbtn-success" href="<?= Url::to(['user/login']);?>"><?=Yii::t('app', 'Getting Started')?></a></p>

        <?php } ?>

    </div>

    <div class="body-content">

    </div>
</div>
