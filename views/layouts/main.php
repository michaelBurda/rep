<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use kartik\sidenav\SideNav;
use lajax\languagepicker\widgets\LanguagePicker;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap reports-site">
    <?php

 NavBar::begin([

        'brandLabel' => Html::img('@web/images/logo_nuwm2.png', ['style' => 'height: 50px'], ['alt'=>Yii::$app->name]),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'nnavbar-inverse navbar-fixed-top',
        ],
    ]);

    echo Nav::widget([
        'items' => [
            [
                'label' => Yii::t('app', 'Site of NUWM'),
                'url' => null,
                'linkOptions' => ['href' => 'http://nuwm.edu.ua/', 'target' => "_blank"]
            ],
            [
                'label' => Yii::t('app', 'Foreign students work centre'),
                'url' => null,
                'linkOptions' => ['href' => 'http://inter.nuwm.edu.ua', 'target' => "_blank"]
            ],
            [
                'label' => Yii::t('app', 'NUWM Wiki'),
                'url' => null,
                'linkOptions' => ['href' => 'http://wiki.nuwm.edu.ua/index.php/%D0%A6%D0%B5%D0%BD%D1%82%D1%80_%D1%80%D0%BE%D0%B1%D0%BE%D1%82%D0%B8_%D0%B7_%D1%96%D0%BD%D0%BE%D0%B7%D0%B5%D0%BC%D0%BD%D0%B8%D0%BC%D0%B8_%D1%81%D1%82%D1%83%D0%B4%D0%B5%D0%BD%D1%82%D0%B0%D0%BC%D0%B8_%D1%82%D0%B0_%D0%BC%D1%96%D0%B6%D0%BD%D0%B0%D1%80%D0%BE%D0%B4%D0%BD%D0%B8%D1%85_%D0%B2%D1%96%D0%B4%D0%BD%D0%BE%D1%81%D0%B8%D0%BD', 'target' => "_blank"]
            ],
        ],
        'options' => ['class' => 'navbar-nav'],
    ]);

    // format top menu items
    if (Yii::$app->user->isGuest) {
        $items = [
            ['label' => Yii::t('app', 'Login'), 'url' => ['/user/login']]
        ];
    }

    if (!Yii::$app->user->isGuest) {
        $items[] = [
            'label' => Yii::t('app', 'Hello') . ' ' . '<b>' . Yii::$app->user->displayName . '</b>',
            'items' => [
                ['label' => Yii::t('app', 'Profile'), 'url' => ['/user/user/view', 'id' => Yii::$app->user->id]],
                '<li class="divider"></li>',
                [
                    'label' => Yii::t('app', 'Logout') . ' (' . Yii::$app->user->displayName . ')',
                    'url' => ['/user/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ],
            ],
        ];
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => $items,
    ]);

    ?>

    <div class="navbar-text pull-right" id="language-picker-container">
        <?=
        LanguagePicker::widget([
            'skin' => LanguagePicker::SKIN_DROPDOWN,
            'size' => LanguagePicker::SIZE_LARGE,
            'parentTemplate' => '<div class="language-picker dropdown-list {size}"><div>{activeItem}<ul id="language-picker-ul">{items}</ul></div></div>',
        ]);
        ?>
    </div>

    <?php NavBar::end(); ?>

    <?php if (Yii::$app->user->isGuest) { ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <?= $content ?>
        </div>

    <?php } else { ?>
        <div id="wrapper">

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <?= SideNav::widget([
                    'type' => SideNav::TYPE_DEFAULT,
                    'encodeLabels' => false,
                    'items' => Yii::$app->mainMenu->items,
                ]);
                ?>
            </div>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-lg-12">
                            <?= Breadcrumbs::widget([
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            ]) ?>
                            <?= $content ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->

        </div>
    <?php } ?>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; NUWM developers team 2016</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
