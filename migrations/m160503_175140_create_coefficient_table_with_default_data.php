<?php

use yii\db\Migration;

class m160503_175140_create_coefficient_table_with_default_data extends Migration
{
    protected $tn_coefficient = '{{%coefficient}}';

    public function safeUp()
    {
        $this->createTable($this->tn_coefficient, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->string()->notNull(),
            'value' => $this->double()->notNull()
        ]);

        $coefficients = [
            [
                1,
                'k1',
                'Коефіцієнт академічної мобільності',
                0.2
            ],
            [
                2,
                'k2',
                'Коефіцієнт міжнародних публікацій',
                0.2
            ],
            [
                3,
                'k3',
                'Коефіцієнт активності міжнародного співробітництва',
                0.2
            ],
            [
                4,
                'k4',
                'Коефіцієнт активності на міжнародних ринках освітніх послуг',
                0.2
            ],
            [
                5,
                'k5',
                'Коефіцієнт ефективності міжнародної діяльності',
                0.2
            ],
        ];
        $this->batchInsert($this->tn_coefficient, ['id', 'name', 'description', 'value'], $coefficients);
    }

    public function safeDown()
    {
        $this->dropTable($this->tn_coefficient);

    }
}
