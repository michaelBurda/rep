<?php

use yii\db\Migration;

class m160329_180521_create_table_institute_with_test_data extends Migration
{
    protected $tn_institute = '{{%institute}}';
//    protected $tn_department;
//    protected $tn_user_technologies;



//    public function __construct()
//    {
//        parent::__construct();
//        $this->tn_institute = '{{%institute}}';
//        $this->tn_technology = '{{%technology}}';
//        $this->tn_user_technologies = '{{%user_technologies}}';
//    }

    public function safeUp()
    {
        $this->createTable($this->tn_institute, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()
        ]);

//        $this->createTable($this->tn_user_technologies, [
//            'id' => $this->primaryKey(),
//            'user_id' => $this->integer()->notNull(),
//            'technology_id' => $this->integer()->notNull()
//        ]);

//        $this->addForeignKey('technology_user_id', $this->tn_user_technologies, 'user_id', $this->tn_user, 'id', 'NO ACTION', 'NO ACTION');
//        $this->addForeignKey('technology_id', $this->tn_user_technologies, 'technology_id', $this->tn_technology, 'id', 'NO ACTION', 'NO ACTION');

        $institutes = [
            [1, 'Водного господарства та природооблаштування'],
            [2, 'Механічний'],
            [3, 'Агроекології та землеустрою'],
            [4, 'Будівництва та архітектури'],
            [5, 'Автоматики, кібернетики та обчислювальної техніки'],
            [6, 'Економіки та менеджменту'],
            [7, 'Права'],
        ];
        $this->batchInsert($this->tn_institute, ['id', 'name'], $institutes);

//        $userTechnologies = [
//            [3, 1],
//            [3, 2],
//            [3, 3],
//            [3, 4],
//            [3, 7],
//        ];
//        $this->batchInsert($this->tn_user_technologies, ['user_id', 'technology_id'], $userTechnologies);

    }

    public function safeDown()
    {
//        $this->dropForeignKey('technology_user_id', $this->tn_user_technologies);
//        $this->dropForeignKey('technology_id', $this->tn_user_technologies);
          $this->dropTable($this->tn_institute);
 //       $this->dropTable($this->tn_user_technologies);
    }
}
