<?php

use yii\db\Migration;

/**
 * Handles the creation for table `report_permissions`.
 */
class m160513_204215_create_report_permissions extends Migration
{
    protected $tn_users = '{{%user}}';
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        // add permissions
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['report.create', 2, 'Create report'],
            ['report.read',   2,   'Read report'],
            ['report.update', 2, 'Update report'],
            ['report.delete', 2, 'Delete report'],
            ['charts.read', 2, 'Read charts'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // Admin permissions
            ['Admin', 'report.create'],
            ['Admin', 'report.read'],
            ['Admin', 'report.update'],
            ['Admin', 'report.delete'],
            ['Admin', 'charts.read'],

            // Employee permissions
            ['Employee', 'report.create'],
            ['Employee', 'report.read'],
            ['Employee', 'report.update'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = 'report.create'");
        $this->delete($this->tn_auth_item, "name = 'report.read'");
        $this->delete($this->tn_auth_item, "name = 'report.update'");
        $this->delete($this->tn_auth_item, "name = 'report.delete'");
        $this->delete($this->tn_auth_item, "name = 'charts.read'");

        $this->delete($this->tn_auth_item_child, "child = 'report.create'");
        $this->delete($this->tn_auth_item_child, "child = 'report.read'");
        $this->delete($this->tn_auth_item_child, "child = 'report.update'");
        $this->delete($this->tn_auth_item_child, "child = 'report.delete'");
        $this->delete($this->tn_auth_item_child, "child = 'charts.read'");
    }
}
