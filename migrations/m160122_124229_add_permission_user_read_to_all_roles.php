<?php

use yii\db\Schema;
use yii\db\Migration;

class m160122_124229_add_permission_user_read_to_all_roles extends Migration
{
    protected $t_auth_item_child = '{{%auth_item_child}}';

    public function up()
    {
        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
          ['Employee', 'user.read'],
        ];
        $this->batchInsert($this->t_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function down()
    {
        // delete permission "user.read" from role
        $this->delete($this->t_auth_item_child, "parent = 'Employee' AND child = 'user.read'");
    }

}
