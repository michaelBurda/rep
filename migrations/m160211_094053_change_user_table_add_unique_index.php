<?php

use yii\db\Schema;
use yii\db\Migration;

class m160211_094053_change_user_table_add_unique_index extends Migration
{
    protected $tn_user = '{{%user}}';

    public function up()
    {
        $this->createIndex('user_unique_auth_key', $this->tn_user, 'auth_key', true);
        $this->createIndex('user_unique_access_token', $this->tn_user, 'access_token', true);
    }

    public function down()
    {
        $this->dropIndex('user_unique_auth_key', $this->tn_user);
        $this->dropIndex('user_unique_access_token', $this->tn_user);
    }

}
