<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_092956_add_test_accounts_roles_and_permisions extends Migration
{
    protected $tn_users = '{{%user}}';
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        // add test accounts
        $pass = 'asdf';
        $passHash = Yii::$app->security->generatePasswordHash($pass);

        $userRows = ['id', 'email', 'username', 'password'];
        $users = [
            [1, 'admin@mail.com', 'admin', $passHash],
            [2, 'employee@mail.com', 'employee',   $passHash],
        ];
        $this->batchInsert($this->tn_users, $userRows, $users);

        // add roles
        $roleRows = ['name', 'type'];
        $roles = [
            ['Admin', 1],
            ['Employee', 1],
        ];
        $this->batchInsert($this->tn_auth_item, $roleRows, $roles);

        // add permissions
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            // user permissions
            ['user.create', 2, 'Create user'],
            ['user.read',   2, 'Read user'],
            ['user.update', 2, 'Update user'],
            ['user.delete', 2, 'Delete user'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // admin permissions
            ['Admin', 'user.create'],
            ['Admin', 'user.read'],
            ['Admin', 'user.update'],
            ['Admin', 'user.delete'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);

        $userRolesRows = ['item_name', 'user_id'];
        $userRoles = [
            ['Admin',       1],
            ['Employee',  2],
        ];
        $this->batchInsert($this->tn_auth_assignment, $userRolesRows, $userRoles);

    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->execute('TRUNCATE TABLE ' . $this->tn_users);
        $this->execute('TRUNCATE TABLE ' . $this->tn_auth_item);
        $this->execute('TRUNCATE TABLE ' . $this->tn_auth_item_child);
        $this->execute('TRUNCATE TABLE ' . $this->tn_auth_assignment);
    }
}
