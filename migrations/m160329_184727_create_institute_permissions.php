<?php

use yii\db\Migration;

class m160329_184727_create_institute_permissions extends Migration
{
    protected $tn_users = '{{%user}}';
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        // add permissions
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['institute.create', 2, 'Create institute'],
            ['institute.read',   2,   'Read institute'],
            ['institute.update', 2, 'Update institute'],
            ['institute.delete', 2, 'Delete institute'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // Admin permissions
            ['Admin', 'institute.create'],
            ['Admin', 'institute.read'],
            ['Admin', 'institute.update'],
            ['Admin', 'institute.delete'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = 'institute.create'");
        $this->delete($this->tn_auth_item, "name = 'institute.read'");
        $this->delete($this->tn_auth_item, "name = 'institute.update'");
        $this->delete($this->tn_auth_item, "name = 'institute.delete'");

        $this->delete($this->tn_auth_item_child, "child = 'institute.create'");
        $this->delete($this->tn_auth_item_child, "child = 'institute.read'");
        $this->delete($this->tn_auth_item_child, "child = 'institute.update'");
        $this->delete($this->tn_auth_item_child, "child = 'institute.delete'");
    }

}