<?php

use yii\db\Schema;
use yii\db\Migration;

class m160125_135554_create_user_all_permission extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';

    public function safeUp()
    {
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['user.all', 2, 'Full access to user'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Admin', 'user.all'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = 'user.all'");
        $this->delete($this->tn_auth_item_child, "child = 'user.all'");
    }

}
