<?php

use yii\db\Migration;

class m160330_200248_create_department_permissions extends Migration
{
    protected $tn_users = '{{%user}}';
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        // add permissions
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['department.create', 2, 'Create department'],
            ['department.read',   2,   'Read department'],
            ['department.update', 2, 'Update department'],
            ['department.delete', 2, 'Delete department'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // Admin permissions
            ['Admin', 'department.create'],
            ['Admin', 'department.read'],
            ['Admin', 'department.update'],
            ['Admin', 'department.delete'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = 'department.create'");
        $this->delete($this->tn_auth_item, "name = 'department.read'");
        $this->delete($this->tn_auth_item, "name = 'department.update'");
        $this->delete($this->tn_auth_item, "name = 'department.delete'");

        $this->delete($this->tn_auth_item_child, "child = 'department.create'");
        $this->delete($this->tn_auth_item_child, "child = 'department.read'");
        $this->delete($this->tn_auth_item_child, "child = 'department.update'");
        $this->delete($this->tn_auth_item_child, "child = 'department.delete'");
    }

}