<?php

use yii\db\Schema;
use yii\db\Migration;
use app\modules\user\models\User;

class m160121_130432_create_employee_permissions extends Migration
{
    protected $tn_users = '{{%user}}';
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        // add permissions
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['user.employeesList', 2, 'View users as employees (without CRUD functional)'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // Employee permissions
            ['Employee', 'user.employeesList'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = 'user.employeesList'");
        $this->delete($this->tn_auth_item_child, "child = 'user.employeesList'");
    }

}
