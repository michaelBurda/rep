<?php

use yii\db\Migration;

class m160509_155815_create_coefficient_permissions extends Migration
{
    protected $tn_users = '{{%user}}';
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        // add permissions
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            ['coefficient.read',   2,   'Read coefficient'],
            ['coefficient.update', 2, 'Update coefficient'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            // Admin permissions
            ['Admin', 'coefficient.read'],
            ['Admin', 'coefficient.update'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = 'coefficient.read'");
        $this->delete($this->tn_auth_item, "name = 'coefficient.update'");

        $this->delete($this->tn_auth_item_child, "child = 'coefficient.read'");
        $this->delete($this->tn_auth_item_child, "child = 'coefficient.update'");
    }
}
