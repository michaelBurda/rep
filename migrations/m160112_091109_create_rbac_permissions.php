<?php

use yii\db\Schema;
use yii\db\Migration;

class m160112_091109_create_rbac_permissions extends Migration
{
    protected $tn_auth_item = '{{%auth_item}}';
    protected $tn_auth_item_child = '{{%auth_item_child}}';
    protected $tn_auth_assignment = '{{%auth_assignment}}';

    public function safeUp()
    {
        // add permissions
        $permissionsRows = ['name', 'type', 'description'];
        $permissions = [
            // rbac route
            ['/rbac/*', 2, 'Role based access control'],
        ];
        $this->batchInsert($this->tn_auth_item, $permissionsRows, $permissions);

        // add new permissions to admin role
        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Admin', '/rbac/*'],
        ];
        $this->batchInsert($this->tn_auth_item_child, $rolePermissionsRows, $rolePermissions);

    }

    public function safeDown()
    {
        $this->delete($this->tn_auth_item, "name = '/rbac/*'");

        $this->delete($this->tn_auth_item_child, "parent = 'Admin' AND child = '/rbac/*'");
    }

}
