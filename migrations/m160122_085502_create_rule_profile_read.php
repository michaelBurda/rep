<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\rbac;

class m160122_085502_create_rule_profile_read extends Migration
{
    protected $t_rule = '{{%auth_rule}}';
    protected $t_auth_item = '{{%auth_item}}';
    protected $t_auth_item_child = '{{%auth_item_child}}';
    protected $rule;

    public function __construct()
    {
        parent::__construct();
        $this->rule = new \app\modules\rbac\UserProfileRule();
    }
    public function up()
    {
        /*
         * Create UserProfileRule
         */
        $this->insert($this->t_rule, [
           'name' => $this->rule->name,
           'data' => serialize($this->rule),
        ]);
        /*
         * Create permission "user.profileView"
         */
        $this->insert($this->t_auth_item, [
          'name' => "user.profileView",
          'type' => 2,
          'description' => 'User can see his profile',
          'rule_name' => $this->rule->name,
        ]);
        /*
         * Add permission to roles
         */
        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Admin', 'user.profileView'],
            ['Employee', 'user.profileView'],
        ];
        $this->batchInsert($this->t_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function down()
    {
        // delete rule "profileView"
        $this->delete($this->t_rule, "name = '" . $this->rule->name . "'");
        // delete permission "user.profileView"
        $this->delete($this->t_auth_item, "name = 'user.profileView'");
        // delete permission "user" from role
        $this->delete($this->t_auth_item_child, "parent = 'Employee' AND child = 'user.profileView'");
        $this->delete($this->t_auth_item_child, "parent = 'Admin' AND child = 'user.profileView'");
    }
}
