<?php

use yii\db\Schema;
use yii\db\Migration;

class m160211_124558_add_test_access_tokens_to_users extends Migration
{
    protected $tn_user = '{{%user}}';

    public function safeUp()
    {
        for ($userId = 1; $userId <= 7; $userId++) {
            $token = hash('sha512', time() . rand());
            $this->update($this->tn_user, ['access_token' => $token], ['id' => $userId]);
        }
    }

    public function safeDown()
    {
        $this->update($this->tn_user, ['access_token' => null]);
    }

}
