<?php

use yii\db\Migration;

class m160517_210032_create_report_manage_rule extends Migration
{
    protected $t_rule = '{{%auth_rule}}';
    protected $t_auth_item = '{{%auth_item}}';
    protected $t_auth_item_child = '{{%auth_item_child}}';
    protected $rule;

    public function __construct()
    {
        parent::__construct();
        $this->rule = new \app\modules\rbac\ReportManageRule();
    }

    public function up()
    {
        /*
         * Create ReportManageRule
         */
        $this->insert($this->t_rule, [
            'name' => $this->rule->name,
            'data' => serialize($this->rule),
        ]);
        /*
         * Create permission "report.manage"
         */
        $this->insert($this->t_auth_item, [
            'name' => "report.manage",
            'type' => 2,
            'description' => 'User can manage his report',
            'rule_name' => $this->rule->name,
        ]);
        /*
         * Add permission to roles
         */
        $rolePermissionsRows = ['parent', 'child'];
        $rolePermissions = [
            ['Employee', 'report.manage'],
            ['Admin', 'report.manage'],
        ];

        $this->batchInsert($this->t_auth_item_child, $rolePermissionsRows, $rolePermissions);
    }

    public function down()
    {
        // delete rule "ReportManage"
        $this->delete($this->t_rule, "name = '" . $this->rule->name . "'");
        // delete permission "report.manage"
        $this->delete($this->t_auth_item, "name = 'report.manage'");
        // delete permission "report.manage" from role
        $this->delete($this->t_auth_item_child, "parent = 'Employee' AND child = 'report.manage'");
        $this->delete($this->t_auth_item_child, "parent = 'Admin' AND child = 'report.manage'");
    }
}
