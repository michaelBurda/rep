<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_133203_create_profile_table extends Migration
{
    protected $tn_user = '{{%user}}';
    protected $tn_profile = '{{%profile}}';

    public function up()
    {
        $this->createTable($this->tn_profile, [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'middle_name' => $this->string(),
            'institute_id' => $this->integer()->notNull(),
            'department_id' => $this->integer()->notNull(),
        ]);

        $this->addColumn($this->tn_user, 'profile_id', $this->integer());
        $this->addForeignKey('user_profile_id', $this->tn_user, 'profile_id', $this->tn_profile, 'id', 'NO ACTION', 'NO ACTION');

        //$this->addForeignKey('FK_profile_institute_id', $this->tn_profile, 'institute_id', $this->tn_institute, 'id', 'CASCADE', 'SET NULL');
        //$this->addForeignKey('FK_profile_department_id', $this->tn_profile, 'department_id', $this->tn_department, 'id', 'CASCADE', 'SET NULL');

        // add profiles for test accounts
        $profileRows = [
            'id',
            'first_name',
            'last_name',
            'middle_name',
            'institute_id',
            'department_id',
        ];
        $profiles = [
            [
                1,
                'admin first name',
                'admin last name',
                'admin middle name',
                NULL,
                NULL,
            ],
            [
                2,
                'employee first name',
                'employee last name',
                'employee middle name',
                1,
                1,
            ],
        ];
        $this->batchInsert($this->tn_profile, $profileRows, $profiles);

        $this->update($this->tn_user, ['profile_id' => 1], 'id = 1');
        $this->update($this->tn_user, ['profile_id' => 2], 'id = 2');

    }

    public function down()
    {
        $this->dropForeignKey('user_profile_id', $this->tn_user);
        $this->dropColumn($this->tn_user, 'profile_id');

        $this->dropTable($this->tn_profile);
    }
}
