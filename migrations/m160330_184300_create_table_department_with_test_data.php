<?php

use yii\db\Migration;

class m160330_184300_create_table_department_with_test_data extends Migration
{
    protected $tn_department;
    protected $tn_institute;

    public function __construct()
    {
        parent::__construct();
        $this->tn_institute = '{{%institute}}';
        $this->tn_department = '{{%department}}';
    }

    public function safeUp()
    {
        $this->createTable($this->tn_department, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'institute_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('FK_department_institute_id', $this->tn_department, 'institute_id', $this->tn_institute, 'id', 'CASCADE', 'CASCADE');

        $department = [
            // nni 1
            [1, 'Водогосподарського будівництва та експлуатації гідромеліоративних систем', 1],
            [2, 'Водогосподарської екології, гідрології та гідравліки', 1],
            [3, 'Гідроенергетики, теплоенергетики та гідравлічних машин', 1],
            [4, 'гідротехнічного будівництва', 1],
            [5, 'Інженерної геології та гідрогеології', 1],
            [6, 'Природооблаштування та гідромеліорацій', 1],

            // nni 2
            [7, 'Автомобілів та автомобільного господарства', 2],
            [8, 'Будівельних, дорожніх, меліоративних, сільськогосподарських машин і обладнання', 2],
            [9, 'Розробки родовищ та видобування корисних копалин', 2],
            [10, 'Теоретичної механіки, інженерної графіки та машинознавства', 2],
            [11, 'Транспортних технологій і технічного сервісу', 2],

            // nni 3
            [12, 'Агрохімії, ґрунтознавства та землеробства', 3],
            [13, 'Водних біоресурсів', 3],
            [14, 'Геодезії та картографії', 3],
            [15, 'Екології', 3],
            [16, 'Здоров\'я людини і фізичної реабілітації', 3],
            [17, 'Землеустрою, кадастру, моніторингу земель та геоінформатики', 3],
            [18, 'Туризму', 3],
            [19, 'Хімії та фізики', 3],
            [20, 'Теорії та методики фізичного виховання', 3],

            // nni 4
            [21, 'Автомобільних доріг, основ та фундаментів', 4],
            [22, 'Архітектури та середовищного дизайну', 4],
            [23, 'Водопостачання, водовідведення та бурової справи', 4],
            [24, 'Міського будівництва та господарства', 4],
            [25, 'Мостів і тунелів, опору матеріалів і будівельної механіки', 4],
            [26, 'Основ архітектурного проектування, конструювання та графіки', 4],
            [27, 'Охорони праці та безпеки життєдіяльності', 4],
            [28, 'Промислового, цивільного будівництва та інженерних споруд', 4],
            [29, 'Теплогазопостачання, вентиляції та санітарної техніки', 4],
            [30, 'Технології будівельних виробів і матеріалознавства', 4],

            // nni 5
            [31, 'Автоматизації, електротехнічних та комп’ютерно-інтегрованих технологій', 5],
            [32, 'Вищої математики', 5],
            [33, 'Обчислювальної техніки', 5],
            [34, 'Прикладної математики', 5],

            // nni 6
            [35, 'Економіки підприємства', 6],
            [36, 'Економічної кібернетики', 6],
            [37, 'Економічної теорії', 6],
            [38, 'Іноземних мов', 6],
            [39, 'Маркетингу', 6],
            [40, 'Менеджменту', 6],
            [41, 'Міжнародної економіки', 6],
            [42, 'Обліку і аудиту', 6],
            [43, 'Трудових ресурсів і підприємництва', 6],
            [44, 'Українознавства', 6],
            [45, 'Філософії', 6],
            [46, 'Фінансів та економіки природокористування', 6],

            // nni 7
            [47, 'Державного управління, документознавства та інформаційної діяльності', 7],
            [48, 'Конституційного права та галузевих дисциплін', 7],
            [49, 'Міжнародного права та юридичної журналістики', 7],
            [50, 'Спеціальних юридичних дисциплін', 7],
            [51, 'Суспільних дисциплін', 7],
        ];
        $this->batchInsert($this->tn_department, ['id', 'name', 'institute_id'], $department);

    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_department_institute_id', $this->tn_department);
        $this->dropTable($this->tn_department);
    }
}