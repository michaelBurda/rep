<?php

use yii\db\Schema;
use yii\db\Migration;

class m160103_175036_crate_user_table extends Migration
{
    protected $tn_users = '{{%user}}';

    public function up()
    {
        $this->createTable($this->tn_users, [
            'id' => $this->primaryKey(),
            'email' => $this->string(),
            'username' => $this->string(),
            'password' => $this->string(),
            'auth_key' => $this->string(),
            'access_token' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1)
        ]);
    }

    public function down()
    {
        $this->dropTable($this->tn_users);
    }
}
