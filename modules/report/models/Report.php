<?php

namespace app\modules\report\models;

use app\modules\coefficient\models\Coefficient;
use app\modules\user\models\User;
use app\modules\user\models\Profile;
use app\modules\department\models\Department;
use app\modules\institute\models\Institute;
use ReflectionClass;
use yii\helpers\Inflector;
use Yii;

/**
 * This is the model class for table "report".
 *
 * @property integer $id
 * @property integer $author_id
 * @property User $author
 * @property string $created_at
 * @property integer $n_ukr
 * @property integer $n_inoz
 * @property integer $n_vn
 * @property integer $n_stasp
 * @property integer $k_mimonstat_1
 * @property integer $k_mimonstat_2
 * @property integer $n_dog
 * @property integer $n_mzah
 * @property integer $n_mstr
 * @property integer $n_nmo
 * @property integer $n_inavch_bak
 * @property integer $n_inavch_mag
 * @property integer $n_inavch_asp
 * @property integer $n_inavch_doc
 * @property integer $n_inavch
 * @property string $k_istud
 * @property string $k_mipr
 * @property string $k_ind
 * @property string $k_lic
 * @property string $k_oblad
 * @property string $k
 */
class Report extends \yii\db\ActiveRecord
{
    const TERM_FIRST = 1;
    const TERM_SECOND = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['period', 'term'], 'required'],
            [['author_id', 'n_ukr', 'n_inoz', 'n_vn', 'n_stasp', 'k_mimonstat_1', 'k_mimonstat_2', 'n_dog', 'n_mzah', 'n_mstr', 'n_nmo', 'n_inavch_bak', 'n_inavch_mag', 'n_inavch_asp', 'n_inavch_doc', 'n_inavch', 'term'], 'integer'],
            [['created_at', 'period'], 'safe'],
            [['period'], 'string'],
            ['term', 'default', 'value' => 1],
            [['k_istud', 'k_mipr', 'k_ind', 'k_lic', 'k_oblad', 'k'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('report', 'ID'),
            'author_id' => Yii::t('report', 'Author'),
            'created_at' => Yii::t('report', 'Created At'),
            'period' => Yii::t('report', 'Academic year'),
            'term' => Yii::t('report', 'Term'),
            'termAsString' => Yii::t('report', 'Term'),
            'authorAsString' => Yii::t('report', 'Author'),
            'instituteAsString' => Yii::t('report', 'Institute'),
            'departmentAsString' => Yii::t('report', 'Department'),
            'n_ukr' => 'Кількість виїздів за кордон українських викладачів, науковців, студентів, аспірантів, докторантів, стажистів від підрозділу',
            'n_inoz' => 'Кількість приїздів до підрозділу іноземних викладачів, наукових співробітників, студентів, аспірантів, докторантів, стажистів',
            'n_vn' => 'Загальна кількість викладачів та наукових співробітників підрозділу',
            'n_stasp' => 'Загальна кількість студентів, аспірантів, докторантів, стажистів підрозділу',
            'k_mimonstat_1' => 'Кількість наукових статей, виданих співробітниками, аспірантами та докторантами кафедри у закордонних виданнях',
            'k_mimonstat_2' => 'Кількість наукових публікацій викладачів, науковців, аспірантів та докторантів кафедри, зареєстрованих в базі даних Scopus',
            'n_dog' => 'Кількість діючих угод та договорів університету із зарубіжними університетами-партнерами за профілем кафедри',
            'n_mzah' => 'Кількість проведених протягом року міжнародних заходів (конференцій, семінарів, фахових літніх шкіл, конкурсів, студентських практик для груп, змагань)',
            'n_mstr' => 'Кількість угод та договорів університету за програмою "Подвійний диплом" за напрямом (спеціальністю) кафедри',
            'n_nmo' => 'Кількість представництв у міжнародних організаціях',
            // Кількість інозекмних громадян, які навчаються за напрямком (спеціальністю) кафедри:
            'n_inavch_bak' => 'за ОКР "Бакалавр"',
            'n_inavch_mag' => 'за ОКР "Магістр"',
            'n_inavch_asp' => 'у аспірантурі',
            'n_inavch_doc' => 'у докторантурі',
            'n_inavch' => 'Кількість інозекмних громадян, які навчаються за напрямком (спеціальністю) кафедри',
            'k_istud' => 'Кошти за навчання іноземних студентів, аспірантів, докторантів, стажистів',
            'k_mipr' => 'Сума фінансових надходжень, залучених кафедрою до бюджету університету за виконання міжнародних проектів/грантів',
            'k_ind' => 'Кошти індивідуальних грантів, стипендій, трудових угод з іноземних джерел',
            'k_lic' => 'Кошти віж продажу ліцензій за кордон',
            'k_oblad' => 'Вартість поставленого з-за кордону обладнання, книжок, програмних продуктів, інших матеріальних цінностей, кошти від благодійної допомоги',
            'k' => 'Загальний обсяг надходжень всіх типів до бюджету підрозділу',

            'm1' => 'Індекс академічної мобільності',
            'm2' => 'Індекс міжнародних публікацій',
            'm3' => 'Індекс активності міжнародного співробітництва',
            'm4' => 'Індекс активності на міжнародних ринках освітніх послуг',
            'm5' => 'Індекс ефективності міжнародної діяльності',
            'md' => Yii::t('report', 'International activity'),
        ];
    }

    public function getAuthor(){
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    public function getAuthorAsString(){
        return $this->author->fullName;
    }

    public function getInstituteAsString(){
        return $this->author->instituteAsString;
    }

    public function getDepartmentAsString(){
        return $this->author->departmentAsString;
    }

    public function getTermAsString() {
        $terms = $this->termDropdown();
        return $terms[$this->term];
    }

    public static function termAsArray() {
        $result = [];
        $constPrefix = "TERM_";

        // create a reflection class to get constants
        $reflClass = new ReflectionClass(get_called_class());
        $constants = $reflClass->getConstants();

        // check for status constants (e.g., TERM_FIRST)
        foreach ($constants as $constantName => $constantValue) {
//            // add prettified name to dropdown
            if (strpos($constantName, $constPrefix) === 0) {
                $prettyName = str_replace($constPrefix, "", $constantName);
                $prettyName = Inflector::humanize(strtolower($prettyName));

                $result[] = [
                    'id' => $constantValue,
                    'name' => Yii::t('report', $prettyName)
                ];
            }
        }

        return $result;
    }

    public static function termDropdown() {
        $dropdown = [];
        $constPrefix = "TERM_";

        // create a reflection class to get constants
        $reflClass = new ReflectionClass(get_called_class());
        $constants = $reflClass->getConstants();

        // check for status constants (e.g., TERM_FIRST)
        foreach ($constants as $constantName => $constantValue) {
//            // add prettified name to dropdown
            if (strpos($constantName, $constPrefix) === 0) {
                $prettyName = str_replace($constPrefix, "", $constantName);
                $prettyName = Inflector::humanize(strtolower($prettyName));
                $dropdown[$constantValue] = Yii::t('report', $prettyName);
            }
        }

        return $dropdown;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($insert){
                $this->author_id = Yii::$app->user->id;
                $this->created_at = date('Y-m-d');
            }

            if($this->n_vn == 0)
                $this->n_vn = 1;
            if($this->n_stasp == 0)
                $this->n_stasp = 1;
            if($this->k == 0)
                $this->k = 1;

            $this->m1 = ($this->n_ukr + $this->n_inoz) / ($this->n_vn + $this->n_stasp);
            $this->m2 = ($this->k_mimonstat_1 + $this->k_mimonstat_2) / $this->n_vn;
            $this->m3 = ($this->n_dog + $this->n_mzah + 2 * $this->n_mstr + $this->n_nmo) / $this->n_vn;
            $this->n_inavch = $this->n_inavch_asp + $this->n_inavch_bak + $this->n_inavch_doc + $this->n_inavch_mag;
            $this->m4 = $this->n_inavch / $this->n_stasp;
            $this->m5 = ($this->k_istud + $this->k_mipr + $this->k_ind + $this->k_lic + $this->k_oblad) / $this->k;
            $this->md = Coefficient::getK1() * $this->m1 + Coefficient::getK2() * $this->m2 + Coefficient::getK3() * $this->m3 +
                Coefficient::getK4() * $this->m4 + Coefficient::getK5() * $this->m5;
            return true;
        } else {
            return false;
        }
    }
}
