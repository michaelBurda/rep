<?php

namespace app\modules\report\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\report\models\Report;
use app\modules\user\models\User;
use app\modules\user\models\Profile;
use app\modules\department\models\Department;
use app\modules\institute\models\Institute;

/**
 * ReportSearch represents the model behind the search form about `app\modules\report\models\Report`.
 */
class ReportSearch extends Report
{
    public $authorAsString;
    public $departmentAsString;
    public $instituteAsString;
    public $termAsString;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'author_id', 'n_ukr', 'n_inoz', 'n_vn', 'n_stasp', 'k_mimonstat_1', 'k_mimonstat_2', 'n_dog', 'n_mzah', 'n_mstr', 'n_nmo', 'n_inavch_bak', 'n_inavch_mag', 'n_inavch_asp', 'n_inavch_doc', 'n_inavch'], 'integer'],
            [['created_at', 'authorAsString', 'departmentAsString', 'instituteAsString', 'period', 'termAsString', 'md'], 'safe'],
            [['k_istud', 'k_mipr', 'k_ind', 'k_lic', 'k_oblad', 'k'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_report = Report::tableName();
        $tn_user = User::tableName();
        $tn_profile = Profile::tableName();
        $tn_department = Department::tableName();
        $tn_institute = Institute::tableName();

        $query = Report::find()
            ->from("$tn_report as r")
            ->leftJoin("$tn_user as u", 'u.id = r.author_id')
            ->leftJoin("$tn_profile as p", 'p.id = u.profile_id')
            ->leftJoin("$tn_department as d", 'd.id = p.department_id')
            ->leftJoin("$tn_institute as i", 'i.id = p.institute_id');


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'authorAsString' => [
                    'asc' => ['p.first_name' => SORT_ASC, 'p.last_name' => SORT_ASC, 'p.middle_name' => SORT_ASC],
                    'desc' => ['p.first_name' => SORT_DESC, 'p.last_name' => SORT_DESC, 'p.middle_name' => SORT_DESC],
                ],
                'instituteAsString' => [
                    'asc' => ['i.name' => SORT_ASC],
                    'desc' => ['i.name' => SORT_DESC],
                ],
                'departmentAsString' => [
                    'asc' => ['d.name' => SORT_ASC],
                    'desc' => ['d.name' => SORT_DESC],
                ],
                'period' => [
                    'asc' => ['r.period' => SORT_ASC],
                    'desc' => ['r.period' => SORT_DESC],
                ],
                'termAsString' => [
                    'asc' => ['r.term' => SORT_ASC],
                    'desc' => ['r.term' => SORT_DESC],
                ],
                'md' => [
                    'asc' => ['r.md' => SORT_ASC],
                    'desc' => ['r.md' => SORT_DESC],
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->authorAsString)) {
            $query->andWhere('p.first_name LIKE "%' . $this->authorAsString . '%" ' .
                'OR p.last_name LIKE "%' . $this->authorAsString . '%"' .
                'OR p.middle_name LIKE "%' . $this->authorAsString . '%"'
            );
        }

        if(!empty($this->departmentAsString)){
            $query->andWhere('d.name LIKE "%' . $this->departmentAsString . '%"');
        }

        if(!empty($this->instituteAsString)){
            $query->andWhere('i.name LIKE "%' . $this->instituteAsString . '%"');
        }

        if(!empty($this->md)) {
            if ($c = $this->isComparable($this->md)) {
                $query->andWhere([$c['condition'], 'md', $c['value']]);
            } else {
                $query->andWhere(['md' => $this->md]);
            }
        }

        if(!empty($this->period)) {
            $query->andWhere('r.period LIKE "%' . $this->period . '%"');
        }

        if (!empty($this->termAsString)) {
            $query->andWhere(['r.term' => $this->termAsString]);
        }

        if(!Yii::$app->user->can('Admin')) {
            $query->andWhere(['r.author_id' => Yii::$app->user->id]);
        }

        return $dataProvider;
    }

    // check, is string contain comparable symbols ('>=', '<=', '>', '<')
    // and return false, or ['condition' => '...', 'value' => '...']
    private function isComparable($string) {
        $conditions = ['>=', '<=', '>', '<'];
        foreach ($conditions as $condition) {
            if (strpos($string, $condition) === 0) {
                return [
                    'condition' => $condition,
                    'value' => str_replace($condition, '', $string)
                ];
            }
        }
        return false;
    }
}
