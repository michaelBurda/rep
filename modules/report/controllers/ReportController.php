<?php

namespace app\modules\report\controllers;

use app\modules\department\models\Department;
use app\modules\institute\models\Institute;
use app\modules\user\models\User;
use Yii;
use app\modules\report\models\Report;
use app\modules\report\models\search\ReportSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;

/**
 * ReportController implements the CRUD actions for Report model.
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Report models.
     * @return mixed
     */
    public function actionList()
    {
        if (!Yii::$app->user->can('report.read')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        $searchModel = new ReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $termsArray = Report::termAsArray();

        $actionButtonsTemplate = Yii::$app->actionButtonsHelper->getActionButtonsTemplate('report');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'termsArray' => $termsArray,
            'actionButtonsTemplate' => $actionButtonsTemplate,
        ]);
    }

    /**
     * Displays a single Report model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('report.read')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        $model = $this->findModel($id);

        if (!Yii::$app->user->can('report.manage') && !Yii::$app->user->can('Admin')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Report model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('report.create')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        $model = new Report();

        $terms = Report::termDropdown();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'terms' => $terms
            ]);
        }
    }

    /**
     * Updates an existing Report model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('report.update')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        $model = $this->findModel($id);

        $terms = Report::termDropdown();

        if (!Yii::$app->user->can('report.manage') && !Yii::$app->user->can('Admin')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'terms' => $terms
            ]);
        }
    }

    public function actionCharts()
    {

        $searchModel = new ReportSearch();
        $nextYear = date('Y') + 1;
        $searchModel->period = date('Y') . '-' . $nextYear;
        $searchModel->termAsString = 1;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $terms = Report::termDropdown();

        $institutes = Institute::find()->all();
        $departments = Department::find()->all();
        $reports = $dataProvider->getModels();

        ////////////////// блок обробки данних для кафедр ///////////////////
        $departmentNames = [];
        $departmentValues = [];

        $departmentNamesFinal = [];
        $departmentValuesFinal = [];
        $departmentAveragesFinal = [];

        uasort($reports, function($a, $b)
        {
            $al = $a->md;
            $bl = $b->md;
            if ($al == $bl) {
                return 0;
            }
            return ($al > $bl) ? -1 : 1;
        });

        // для всіх кафедр які здали звіт ставимо їхні МД
        foreach($reports as $report){
            $depName = $report->author->profile->department->name;
            if(!in_array($depName, $departmentNames)) {
                array_push($departmentNames, $report->author->profile->department->name);
                array_push($departmentValues, floatval($report->md));
            }
        }

        // для всіх кафедр які не здали звіт ставимо 0
        foreach($departments as $department){
            if(!in_array($department->name, $departmentNames)){
                array_push($departmentNames, $department->name);
                array_push($departmentValues, 0);
            }
        }

        // визначаємо середнє арифметичне
        $sum = 0;
        $count = count($departmentValues);
        foreach($departmentValues as $value){
            $sum += $value;
        }
        $average = round($sum/$count, 2);

        // визначаємо кращі 10
        for($i = 0; $i < 10; ++$i){
            array_push($departmentNamesFinal, $departmentNames[$i]);
            array_push($departmentValuesFinal, $departmentValues[$i]);
            array_push($departmentAveragesFinal, $average);
        }
        //////////////////////////////////////////////////////////////////
        ////////////////// блок обробки данних для ННІ ///////////////////
        $instituteData = [];

        $instituteNamesFinal = [];
        $instituteValuesFinal = [];
        $instituteAveragesFinal = [];

        // формуємо масив об'єктів у вигляді ім*я-значення
        foreach($institutes as $institute){
            $instituteSum = 0;
            $instituteDepartmentsIds = [];
            $instituteDepartments = $institute->getDepartments();
            foreach($instituteDepartments as $instituteDepartment){
                array_push($instituteDepartmentsIds, $instituteDepartment->id);
            }

            foreach($reports as $report){
                if(in_array($report->author->profile->department->id, $instituteDepartmentsIds)){
                    $instituteSum += $report->md;
                }
            }

            $data = ['instituteName' => $institute->name, 'instituteValue' => $instituteSum/count($instituteDepartments)];
            array_push($instituteData, $data);
        }

        // сортуємо
        uasort($instituteData, function($a, $b)
        {
            $al = $a['instituteValue'];
            $bl = $b['instituteValue'];
            if ($al == $bl) {
                return 0;
            }
            return ($al > $bl) ? -1 : 1;
        });

        // розбиваємо відсортований масив на 2 масиви - імен та значень
        foreach($instituteData as $item){
            array_push($instituteNamesFinal, $item['instituteName']);
            array_push($instituteValuesFinal, $item['instituteValue']);
        }

        // знаходимо середнє арифметичне
        $sumInstitutes = 0;
        foreach($instituteValuesFinal as $value){
            $sumInstitutes += $value;
        }
        $instituteAverage = round($sumInstitutes/count($instituteValuesFinal), 2);

        // формуємо масив значень середнього арифметичного
        for($i = 0; $i < count($instituteNamesFinal); ++$i){
            array_push($instituteAveragesFinal, $instituteAverage);
        }
        //////////////////////////////////////////////////////////////////

        return $this->render('charts', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'terms' => $terms,

            //'userModel' => $userModel,
            //'years' => $years,
            'departmentNames' => $departmentNamesFinal,
            'departmentValues' => $departmentValuesFinal,
            'departmentAverages' => $departmentAveragesFinal,
            'instituteNames' => $instituteNamesFinal,
            'instituteValues' => $instituteValuesFinal,
            'instituteAverages' => $instituteAveragesFinal

        ]);
    }

    /**
     * Deletes an existing Report model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('report.delete')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        $model = $this->findModel($id);

//        if (!Yii::$app->user->can('report.manage') && !Yii::$app->user->can('Admin')) {
//            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
//        }

        $model->delete();

        return $this->redirect(['list']);
    }

    /**
     * Finds the Report model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Report the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Report::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
