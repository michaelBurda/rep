<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\report\models\Report */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reports'), 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if(Yii::$app->user->can('report.update')): ?>
            <?= Html::a(Yii::t('report', 'Update'), ['update', 'id' => $model->id], ['class' => 'nbtn nbtn-success']) ?>
        <?php endif; ?>

        <?php if(Yii::$app->user->can('report.delete')): ?>
            <?= Html::a(Yii::t('report', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'nbtn nbtn-success',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'period',
            'termAsString',
            'authorAsString',
            'instituteAsString',
            'departmentAsString',
            'created_at',
            'n_ukr',
            'n_inoz',
            'n_vn',
            'n_stasp',
            'k_mimonstat_1',
            'k_mimonstat_2',
            'n_dog',
            'n_mzah',
            'n_mstr',
            'n_nmo',
            'n_inavch',
            'n_inavch_bak',
            'n_inavch_mag',
            'n_inavch_asp',
            'n_inavch_doc',
            'k_istud',
            'k_mipr',
            'k_ind',
            'k_lic',
            'k_oblad',
            'k',
            'm1',
            'm2',
            'm3',
            'm4',
            'm5',
            'md',
        ],
    ]) ?>

</div>
