<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\report\models\Report */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
<!--        <div class="col-xs-12 col-sm-6">-->
            <h4 class="nstyle-text">Показники академічної мобільності</h4>
            <hr />

            <?= $form->field($model, 'n_ukr')->textInput() ?>

            <?= $form->field($model, 'n_inoz')->textInput() ?>

            <hr />
            <h4 class="nstyle-text">Показники активності міжнародного співробітництва</h4>
            <hr />

            <?= $form->field($model, 'n_dog')->textInput() ?>

            <?= $form->field($model, 'n_mzah')->textInput() ?>

            <?= $form->field($model, 'n_mstr')->textInput() ?>

            <?= $form->field($model, 'n_nmo')->textInput() ?>

            <hr />
            <h4 class="nstyle-text">Показники ефективності міжнародної діяльності</h4>
            <hr />

            <?= $form->field($model, 'k_istud')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'k_mipr')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'k_ind')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'k_lic')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'k_oblad')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'k')->textInput(['maxlength' => true]) ?>
<!--        </div>-->

<!--        <div class="col-xs-12 col-sm-6">-->
            <h4 class="nstyle-text">Показники міжнародних публікацій</h4>
            <hr />

            <?= $form->field($model, 'k_mimonstat_1')->textInput() ?>

            <?= $form->field($model, 'k_mimonstat_2')->textInput() ?>

            <hr />
            <h4 class="nstyle-text">Показники активності міжнародного співробітництва</h4>
            <hr />
            <label class="control-label">Кількість інозекмних громадян, які навчаються за напрямком (спеціальністю) кафедри:</label>
            <?= $form->field($model, 'n_inavch_bak')->textInput() ?>

            <?= $form->field($model, 'n_inavch_mag')->textInput() ?>

            <?= $form->field($model, 'n_inavch_asp')->textInput() ?>

            <?= $form->field($model, 'n_inavch_doc')->textInput() ?>

            <hr />
            <h4 class="nstyle-text">Загальна інформація</h4>
            <hr />

            <?= $form->field($model, 'n_vn')->textInput() ?>

            <?= $form->field($model, 'n_stasp')->textInput() ?>

            <?= $form->field($model, 'period')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999-9999',
            ]) ?>

            <?= $form->field($model, 'term')->dropDownList($terms) ?>
<!--        </div>-->
    </div>

    <div class="form-group text-center">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('report', 'Create') : Yii::t('report', 'Update'), ['class' => $model->isNewRecord ? 'nbtn nbtn-success' : 'nbtn nbtn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
