<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\report\models\Report */

$this->title = Yii::t('report', 'Create Report');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reports'), 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'terms' => $terms,
    ]) ?>

</div>
