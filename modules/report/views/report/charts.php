<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use kartik\form\ActiveForm;

$this->title = Yii::t('app', 'Charts');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reports'), 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="report-charts">

        <h1><?= Html::encode($this->title) ?></h1>

        <?= Html::a(Yii::t('app', 'Reports'), ['list'], ['class' => 'nbtn nbtn-success']) ?>
        <div style="margin-top: 10px">
            <?php $form = ActiveForm::begin(['method' => 'get']); ?>

            <?= $form->field($searchModel, 'period')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9999-9999',
            ]) ?>

            <?= $form->field($searchModel, 'termAsString')->dropDownList($terms) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('report', 'Search'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <?=
            Highcharts::widget([
                'options' => [
                    'title' => ['text' => Yii::t('report', 'International activity of NUWM departments(') . $searchModel->period . Yii::t('report', ' ay.) (') . $terms[$searchModel->termAsString]  . Yii::t('report', ' term)')],
                    'xAxis' => [
                        'categories' => $departmentNames,
                    ],
                    'yAxis' => [
                        'title' => ['text' => Yii::t('report', 'International activity')]
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'name' => Yii::t('report', 'International activity'),
                            'data' => $departmentValues,
                            'dataLabels' =>
                                [
                                    'enabled' => true,
                                    'rotation' => -90,
                                    'color' => '#FFFFFF',
                                    'align' => 'right',
                                    'format' => '{point.y:.1f}',
                                    'y' => 10,
                                    'style' => ['fontSize' => '13px', 'fontFamily' => 'Verdana, sans-serif']
                                ],
                        ],
                        [
                            'type' => 'spline',
                            'name' => Yii::t('report', 'Average'),
                            'data' => $departmentAverages,
                            'marker' => ['lineWidth' => 2, 'lineColor' => 'orange', 'fillColor' => 'red']
                        ]
                    ]
                ]
            ]);
        ?>

        <?php

        ?>

        <?=
        Highcharts::widget([
            'options' => [
                'title' => ['text' => Yii::t('report', 'International activity of NUWM institutes (') . $searchModel->period . Yii::t('report', ' ay.) (') . $terms[$searchModel->termAsString]  . Yii::t('report', ' term)')],
                'xAxis' => [
                    'categories' => $instituteNames,
                ],
                'yAxis' => [
                    'title' => ['text' => Yii::t('report', 'International activity')]
                ],
                'series' => [
                    [
                        'type' => 'column',
                        'name' => Yii::t('report', 'International activity'),
                        'data' => $instituteValues,
                        'dataLabels' =>
                            [
                                'enabled' => true,
                                'rotation' => -90,
                                'color' => '#FFFFFF',
                                'align' => 'right',
                                'format' => '{point.y:.1f}',
                                'y' => 10,
                                'style' => ['fontSize' => '13px', 'fontFamily' => 'Verdana, sans-serif']
                            ],
                    ],
                    [
                        'type' => 'spline',
                        'name' => Yii::t('report', 'Average'),
                        'data' => $instituteAverages,
                        'marker' => ['lineWidth' => 2, 'lineColor' => 'orange', 'fillColor' => 'red']
                    ]
                ]
            ]
        ]);
        ?>

    </div>
