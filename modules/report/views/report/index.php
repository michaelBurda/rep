<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\report\models\search\ReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Reports');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if(Yii::$app->user->can('report.create') || Yii::$app->user->can('charts.read')): ?>
        <p>
            <?php if(Yii::$app->user->can('report.create')): ?>
                <?= Html::a(Yii::t('report', 'Create Report'), ['create'], ['class' => 'nbtn nbtn-success']) ?>
            <?php endif; ?>
            <?php if(Yii::$app->user->can('charts.read')): ?>
                <?= Html::a(Yii::t('app', 'Charts'), ['charts'], ['class' => 'nbtn nbtn-success']) ?>
            <?php endif; ?>
        </p>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'authorAsString',
            'instituteAsString',
            'departmentAsString',
            'md',
            'period',
            [
                'attribute' => 'termAsString',
                'filter' => ArrayHelper::map($termsArray, 'id', 'name')
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $actionButtonsTemplate,
            ],
        ],
    ]); ?>
</div>
