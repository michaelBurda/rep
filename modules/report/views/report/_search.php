<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\report\models\search\ReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'author_id') ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'n_ukr') ?>

    <?= $form->field($model, 'n_inoz') ?>

    <?php // echo $form->field($model, 'n_vn') ?>

    <?php // echo $form->field($model, 'n_stasp') ?>

    <?php // echo $form->field($model, 'k_mimonstat_1') ?>

    <?php // echo $form->field($model, 'k_mimonstat_2') ?>

    <?php // echo $form->field($model, 'n_dog') ?>

    <?php // echo $form->field($model, 'n_mzah') ?>

    <?php // echo $form->field($model, 'n_mstr') ?>

    <?php // echo $form->field($model, 'n_nmo') ?>

    <?php // echo $form->field($model, 'n_inavch_bak') ?>

    <?php // echo $form->field($model, 'n_inavch_mag') ?>

    <?php // echo $form->field($model, 'n_inavch_asp') ?>

    <?php // echo $form->field($model, 'n_inavch_doc') ?>

    <?php // echo $form->field($model, 'n_inavch') ?>

    <?php // echo $form->field($model, 'k_istud') ?>

    <?php // echo $form->field($model, 'k_mipr') ?>

    <?php // echo $form->field($model, 'k_ind') ?>

    <?php // echo $form->field($model, 'k_lic') ?>

    <?php // echo $form->field($model, 'k_oblad') ?>

    <?php // echo $form->field($model, 'k') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('report', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('report', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
