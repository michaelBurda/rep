<?php

namespace app\modules\institute\models;

use app\modules\department\models\Department;
use Yii;

/**
 * This is the model class for table "institute".
 *
 * @property integer $id
 * @property string $name
 */
class Institute extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'institute';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('institute', 'ID'),
            'name' => Yii::t('institute', 'Name'),
        ];
    }

    public function getDepartments(){
        return Department::find()->where(['institute_id' => $this->id])->all();
    }
}
