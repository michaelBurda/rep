<?php
    return [

        'Create Institute' => 'Create Institute',
        'Institutes' => 'Institutes',
        'Update' => 'Update',
        'Institute' => 'Institute',
        'Delete' => 'Delete',
        'Create' => 'Create',
        'Update institute:' => 'Update institute:',
        'ID' => 'ID',
        'Name' => 'Name',
    ];