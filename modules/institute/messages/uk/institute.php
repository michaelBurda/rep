<?php
return [

    'Create Institute' => 'Створити інститут',
    'Institutes' => 'Навчально-наукові інститути',
    'Update' => 'Редагувати',
    'Institute' => 'Інститут',
    'Delete' => 'Видалити',
    'Create' => 'Створити',
    'Update institute:' => 'Редагувати інститут:',
    'ID' => 'ID',
    'Name' => 'Назва',
];