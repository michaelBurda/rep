<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\institute\models\search\InstituteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('institute', 'Institutes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institute-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can('institute.create')) : ?>
            <?= Html::a(Yii::t('institute', 'Create Institute'), ['create'], ['class' => 'nbtn nbtn-success']) ?>
        <?php endif; ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $actionButtonsTemplate,
            ],
        ],
    ]); ?>

</div>
