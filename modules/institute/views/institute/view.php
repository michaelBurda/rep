<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\institute\models\Institute */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('institute', 'Institutes'), 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institute-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can('institute.update')) : ?>
             <?= Html::a(Yii::t('institute', 'Update'), ['update', 'id' => $model->id], ['class' => 'nbtn nbtn-success']) ?>
        <?php endif; ?>
        <?php if (Yii::$app->user->can('institute.delete')) : ?>
            <?= Html::a(Yii::t('institute', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'nbtn nbtn-success',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
