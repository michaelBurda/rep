<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\institute\models\Institute */

$this->title = Yii::t('institute', 'Update institute:'). ' ' . $model->name;


$this->params['breadcrumbs'][] = ['label' => Yii::t('institute', 'Institutes'), 'url' => ['list']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('institute', 'Update');
?>
<div class="institute-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
