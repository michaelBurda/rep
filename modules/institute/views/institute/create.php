<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\institute\models\Institute */

$this->title = Yii::t('institute', 'Create Institute');
$this->params['breadcrumbs'][] = ['label' => Yii::t('institute', 'Institutes'), 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institute-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
