<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\institute\models\Institute */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="institute-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('institute', 'Create') : Yii::t('institute', 'Update'), ['class' => $model->isNewRecord ? 'nbtn nbtn-success' : 'nbtn nbtn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
