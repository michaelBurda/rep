<?php
return [

    'Create department' => 'Створити кафедру',
    'departments' => 'кафедри',
    'Update' => 'Редагувати',
    'department' => 'кафедру',
    'Delete' => 'Видалити',
    'Create' => 'Створити',
    'Update department:' => 'Редагувати кафедру:',
    'ID' => 'ID',
    'Name' => 'Назва',
    'Departments' => 'Кафедри',
    'Institute' => 'Навчально-науковий інститут',
    '--select institute--' => '--виберіть навчально-науковий інститут--',
];