<?php

namespace app\modules\department\models;

use app\modules\institute\models\Institute;
use Yii;
use ReflectionClass;
use yii\helpers\Inflector;

/**
 * This is the model class for table "department".
 *
 * @property integer $id
 * @property string $name
 * @property integer $institute_id
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'department';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'institute_id'], 'required'],
            [['name'], 'string', 'max' => 100],
            ['institute_id', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('department', 'ID'),
            'name' => Yii::t('department', 'Name'),
            'institute_id' => Yii::t('department', 'Institute'),
            'instituteAsString' => Yii::t('department', 'Institute'),
        ];
    }

    public function getInstitute(){
        return $this->hasOne(Institute::className(), ['id' => 'institute_id']);
    }

    public function getInstituteAsString() {
        return $this->institute->name;
    }
}
