<?php

namespace app\modules\department\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\department\models\Department;
use app\modules\institute\models\Institute;


/**
 * DepartmentSearch represents the model behind the search form about `app\modules\department\models\Department`.
 */
class DepartmentSearch extends Department
{
    public $instituteAsString;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'institute_id'], 'integer'],
            [['name', 'instituteAsString'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $tn_department = Department::tableName();
        $tn_institute = Institute::tableName();

        $query = Department::find()
            ->from("$tn_department as d")
            ->leftJoin("$tn_institute as i", 'i.id = d.institute_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'name' => [
                    'asc' => ['d.name' => SORT_ASC],
                    'desc' => ['d.name' => SORT_DESC],
                    'label' => Yii::t('department', 'Name'),
                ],
                'instituteAsString' => [
                    'asc' => ['i.name' => SORT_ASC],
                    'desc' => ['i.name' => SORT_DESC],
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(!empty($this->instituteAsString)){
            $query->andWhere('i.name LIKE "%' . $this->instituteAsString . '%"');
        }

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
