<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\department\models\Department */

$this->title = Yii::t('department', 'Update department:'). ' ' . $model->name;


$this->params['breadcrumbs'][] = ['label' => Yii::t('department', 'Departments'), 'url' => ['list']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('department', 'Update');
?>
<div class="department-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'institutes' => $institutes,
    ]) ?>

</div>
