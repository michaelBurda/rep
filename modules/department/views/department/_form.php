<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\department\models\Department */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="department-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'institute_id')->widget(Select2::className(), [
        'name' => Yii::t('department', 'Institute'),
        'data' => ArrayHelper::map($institutes, 'id', 'name'),
        'size' => Select2::MEDIUM,
        'options' => ['placeholder' => Yii::t('department', '--select institute--'), 'multiple' => false],
        'pluginOptions' => ['allowClear' => true],
    ]);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('department', 'Create') : Yii::t('department', 'Update'), ['class' => $model->isNewRecord ? 'nbtn nbtn-success' : 'nbtn nbtn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
