<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\department\models\Department */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Departments'), 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can('department.update')) : ?>
             <?= Html::a(Yii::t('department', 'Update'), ['update', 'id' => $model->id], ['class' => 'nbtn nbtn-success']) ?>
        <?php endif; ?>
        <?php if (Yii::$app->user->can('department.delete')) : ?>
            <?= Html::a(Yii::t('department', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'nbtn nbtn-success',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'instituteAsString',
        ],
    ]) ?>

</div>
