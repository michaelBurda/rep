<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\department\models\search\DepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('department', 'Departments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can('department.create')) : ?>
            <?= Html::a(Yii::t('department', 'Create department'), ['create'], ['class' => 'nbtn nbtn-success']) ?>
        <?php endif; ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'instituteAsString',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $actionButtonsTemplate,
            ],
        ],
    ]); ?>

</div>
