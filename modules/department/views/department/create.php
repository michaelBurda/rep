<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\department\models\Department */

$this->title = Yii::t('department', 'Create department');
$this->params['breadcrumbs'][] = ['label' => Yii::t('department', 'Departments'), 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'institutes' => $institutes,
    ]) ?>

</div>
