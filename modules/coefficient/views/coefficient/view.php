<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\coefficient\models\Coefficient */

$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coefficients'), 'url' => ['list']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coefficient-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('coefficient', 'Update'), ['update', 'id' => $model->id], ['class' => 'nbtn nbtn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'description',
            'value',
        ],
    ]) ?>

</div>
