<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\coefficient\models\Coefficient */

$this->title = Yii::t('coefficient', 'Update coefficient:'). ' ' . $model->description;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coefficients'), 'url' => ['list']];
$this->params['breadcrumbs'][] = ['label' => $model->description, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('coefficient', 'Update');
?>
<div class="coefficient-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
