<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\coefficient\models\Coefficient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coefficient-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'value')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('coefficient', 'Create') : Yii::t('coefficient', 'Update'), ['class' => $model->isNewRecord ? 'nbtn nbtn-success' : 'nbtn nbtn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
