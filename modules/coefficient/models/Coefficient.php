<?php

namespace app\modules\coefficient\models;

use Yii;

/**
 * This is the model class for table "coefficient".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property double $value
 */
class Coefficient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coefficient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'value'], 'required'],
            [['value'], 'number'],
            [['name', 'description'], 'string', 'max' => 255],
            [['value'], function ($attribute, $params) {
                if ((self::getCoefficientsSum() + $this->value) > 1) {
                    $this->addError($attribute, Yii::t('coefficient', 'The sum of all coefficients must be less than 1'));
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('coefficient', 'ID'),
            'name' => Yii::t('coefficient', 'Name'),
            'description' => Yii::t('coefficient', 'Description'),
            'value' => Yii::t('coefficient', 'Value'),
        ];
    }

    public static function getK1(){
        return self::findOne(['id' => 1]);
    }

    public static function getK2(){
        return self::findOne(['id' => 2]);
    }

    public static function getK3(){
        return self::findOne(['id' => 3]);
    }

    public static function getK4(){
        return self::findOne(['id' => 4]);
    }

    public static function getK5(){
        return self::findOne(['id' => 5]);
    }

    public function getCoefficientsSum(){
        $sum = 0;
        $coefficients = self::find()->all();
        foreach($coefficients as $coefficient){
            if($coefficient->id != $this->id)
            $sum += $coefficient->value;
        }
        return $sum;
    }
}
