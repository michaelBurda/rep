<?php
return [
    'The sum of all coefficients must be less than 1' => 'Сума всіх коефіцієнтів має бути меншою за 1',
    'ID' => 'ID',
    'Name' => 'Назва',
    'Description' => 'Назва',
    'Value' => 'Значення',
    'Create' => 'Створити',
    'Update' => 'Редагувати',
    'Search' => 'Пошук',
    'Reset' => 'Скинути',
    'Update coefficient:' => 'Редагувати коефіцієнт:',
];