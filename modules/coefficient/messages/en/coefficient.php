<?php
return [
    'The sum of all coefficients must be less than 1' => 'The sum of all coefficients must be less than 1',
    'ID' => 'ID',
    'Name' => 'Name',
    'Description' => 'Description',
    'Value' => 'Value',
    'Create' => 'Create',
    'Update' => 'Update',
    'Search' => 'Search',
    'Reset' => 'Reset',
    'Update coefficient:' => 'Update coefficient:',
];