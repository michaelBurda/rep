<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\form\ActiveForm;


/* @var $this yii\web\View */
/* @var $userModel app\modules\user\models\User */
/* @var $profileModel app\modules\user\models\Profile */
/* @var $form yii\widgets\ActiveForm */
/* @var $roles[] use yii\rbac\Role; */
/* @var $action string */

?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <div class="row">
        <!-- left section -->
        <div class="col-xs-12 col-sm-6">

            <?= $form->field($profileModel, 'first_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($profileModel, 'last_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($profileModel, 'middle_name')->textInput(['maxlength' => true]) ?>

<!--            <?
//                =$form->field($profileModel, 'institute_id')->widget(Select2::className(), [
//                'name' => Yii::t('user', 'Institute'),
//                'data' => ArrayHelper::map($institutes, 'id', 'name'),
//                'size' => Select2::MEDIUM,
//                'options' => ['placeholder' => Yii::t('user', '--select institute--'), 'multiple' => false],
//                'pluginOptions' => ['allowClear' => true],
//            ]);?>
-->

            <?= $form->field($profileModel, 'department_id')->widget(Select2::className(), [
                'name' => Yii::t('user', 'Department'),
                'data' => ArrayHelper::map($departments, 'id', 'name'),
                'size' => Select2::MEDIUM,
                'options' => ['placeholder' => Yii::t('user', '--select department--'), 'multiple' => false],
                'pluginOptions' => ['allowClear' => true],
            ]);?>

        </div>

        <!-- right section -->
        <div class="col-xs-12 col-sm-6">

            <?= $form->field($userModel, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($userModel, 'username')->textInput(['maxlength' => true]) ?>

            <?php
            $passwordOptions = ['maxlength' => true, 'value' => ''];
            if ($userModel->isNewRecord) {
                $passwordOptions['required'] = 'required';
            }
            ?>
            <?= $form->field($userModel, 'password')->passwordInput($passwordOptions) ?>

            <?= $form->field($userModel, 'rolesAsArray')->widget(Select2::className(), [
                'name' => 'rolesAsArray',
                'data' => ArrayHelper::map($roles, 'name', 'name'),
                'size' => Select2::MEDIUM,
                'options' => ['placeholder' => Yii::t('user', '--select role(s)--'), 'multiple' => true],
                'pluginOptions' => ['allowClear' => true],
            ]);?>

            <?= $form->field($userModel, 'status')->dropDownList($statuses) ?>

        </div>
    </div>

    <div class="form-group text-center">
        <?= Html::submitButton($userModel->isNewRecord ? Yii::t('user', 'Create') : Yii::t('user', 'Update'), ['class' => $userModel->isNewRecord ? 'nbtn nbtn-success' : 'nbtn nbtn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
