<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = Yii::t('user', 'Change status');
?>
<div class="user-status-edit">
  <?= $this->render('_form_status', [
    'profile' => $profile,
  ]) ?>
</div>
