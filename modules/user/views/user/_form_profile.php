<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use yii\helpers\Url;
use kartik\select2\Select2;
use kartik\form\ActiveForm;


/* @var $this yii\web\View */
/* @var $userModel app\modules\user\models\User */
/* @var $profileModel app\modules\user\models\Profile */
/* @var $form yii\widgets\ActiveForm */
/* @var $action string */

?>

<div class="user-form">

  <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

  <div class="row">
    <!-- left section -->
    <div class="col-xs-12 col-sm-6">

      <?= $form->field($profileModel, 'first_name')->textInput(['maxlength' => true]) ?>

      <?= $form->field($profileModel, 'last_name')->textInput(['maxlength' => true]) ?>

      <?= $form->field($profileModel, 'middle_name')->textInput(['maxlength' => true]) ?>

      <?= $form->field($userModel, 'username')->textInput(['maxlength' => true]) ?>

      <?php
      $passwordOptions = ['maxlength' => true, 'value' => ''];
      if ($userModel->isNewRecord) {
        $passwordOptions['required'] = 'required';
      }
      ?>
      <!-- <?= $form->field($userModel, 'password')->passwordInput($passwordOptions) ?>-->

    </div>

    <!-- right section -->
    <div class="col-xs-12 col-sm-6">

      <?= $form->field($userModel, 'email')->textInput(['maxlength' => true]) ?>

    </div>
  </div>

  <hr/>

  <div class="form-group text-center">
    <?= Html::submitButton($userModel->isNewRecord ? Yii::t('user', 'Create') : Yii::t('user', 'Update'), ['class' => $userModel->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
