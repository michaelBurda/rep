<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $userModel app\modules\user\models\User */
/* @var $profileModel app\modules\user\models\Profile */
/* @var $roles[] use yii\rbac\Role; */

$this->title = Yii::t('user', 'Update User') . ': ' . $userModel->fullName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['list']];
$this->params['breadcrumbs'][] = ['label' => $userModel->fullName, 'url' => ['view', 'id' => $userModel->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->user->can('user.update') && !Yii::$app->user->can('profile.View')): ?>
        <?= $this->render('_form', [
            'userModel' => $userModel,
            'profileModel' => $profileModel,
            'roles' => $roles,
            'statuses' => $statuses,
            'institutes' => $institutes,
            'departments' => $departments,
        ]) ?>
    <?php else: ?>
        <?= $this->render('_form_profile', [
            'userModel' => $userModel,
            'profileModel' => $profileModel,
        ]) ?>
    <?php endif; ?>

</div>
