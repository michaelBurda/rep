<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use app\modules\user\UserAssets;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\models\search\searchUser */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $actionButtonsTemplate String */
/* @var $additionalButtonsTemplate String */
/* @var $otherUsers[] app\modules\user\models\User */

$this->title = Yii::t('user', 'Users');
$this->params['breadcrumbs'][] = $this->title;

UserAssets::register($this);

?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if (Yii::$app->user->can('user.create')) { ?>
        <p>
            <?= Html::a(Yii::t('user', 'Create User'), ['create'], ['class' => 'nbtn nbtn-success']) ?>
        </p>
    <?php } ?>

    <?php Pjax::begin();?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'fullName',
                'instituteAsString',
                'departmentAsString',

                [
                    'attribute' => 'rolesAsString',
                    'filter' => ArrayHelper::map($roles, 'name', 'name'),
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $actionButtonsTemplate,
                ],
            ]
        ]); ?>
    <?php Pjax::end();?>
</div>
