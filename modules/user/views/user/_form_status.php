<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use \app\modules\user\models\Profile;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-change-status-form">

  <?php $form = ActiveForm::begin(['id' => 'userStatusForm']); ?>

  <div class="form-group">
    <?= Html::submitButton(Yii::t('user', 'Change'), ['class' => 'col-xs-12 btn btn-success']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
