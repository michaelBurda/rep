<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\user\models\Profile;

/* @var $this yii\web\View */
/* @var $userModel app\modules\user\models\User */

$this->title = $userModel->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['list']];
$this->params['breadcrumbs'][] = $userModel->fullName;


?>
<div class="user-view">

    <h1><?= Html::encode($userModel->fullName) ?></h1>

    <p>
        <?php if (Yii::$app->user->can('user.update') || Yii::$app->user->can('user.profileView')) { ?>
            <?= Html::a(Yii::t('user', 'Update'), ['update', 'id' => $userModel->id], ['class' => 'nbtn nbtn-success']) ?>
        <?php } ?>

        <?php if (Yii::$app->user->can('user.delete')) { ?>
            <?= Html::a(Yii::t('user', 'Delete'), ['delete', 'id' => $userModel->id], [
                'class' => 'nbtn nbtn-success',
                'data' => [
                    'confirm' => Yii::t('user', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        <?php } ?>
    </p>
    <?= DetailView::widget([
        'model' => $userModel,
        'attributes' => [
            'profile.first_name',
            'profile.last_name',
            'profile.middle_name',
            'profile.instituteAsString',
            'profile.departmentAsString',
            'username',
            'statusAsString',
            'rolesAsString',
            'email:email',
        ],
    ]) ?>
</div>
