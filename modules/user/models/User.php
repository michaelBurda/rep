<?php
namespace app\modules\user\models;

use app\modules\institute\models\Institute;
use yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\rbac\Role;
use ReflectionClass;
use yii\helpers\Inflector;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property string $access_token
 * @property string $name
 * @property string $fullName
 * @property integer $status
 * @property integer $profile_id
 *
 * @property Profile $profile
 * @property Role[] $roles
 * @property string[] $rolesAsArray
 * @property string $rolesAsString
 * @property string $statusAsString
 * @property string $instituteAsString
 * @property string $departmentAsString
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public static function tableName()
    {
        return 'user';
    }

    public function rules()
    {
        return [
            [['username', 'email'], 'required'],
            [['username', 'email'], 'unique'],
            [['status', 'profile_id'], 'integer'],
            [['email', 'username', 'password', 'auth_key', 'access_token'], 'string', 'max' => 255],
            ['email', 'email'],
            ['status', 'default', 'value' => 1],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'Id'),
            'email' => Yii::t('user', 'Email'),
            'username' => Yii::t('user', 'Username'),
            'password' => Yii::t('user', 'Password'),
            'auth_key' => Yii::t('user', 'Auth key'),
            'access_token' => Yii::t('user', 'Access token'),
            'fullName' => Yii::t('user', 'Name'),
            'status' => Yii::t('user', 'Status'),
            'profile_id' => Yii::t('user', 'Profile id'),
            'rolesAsArray' => Yii::t('user', 'Roles'),
            'rolesAsString' => Yii::t('user', 'Roles'),
            'statusAsString' => Yii::t('user', 'Status'),
            'instituteAsString' => Yii::t('user', 'Institute'),
            'departmentAsString' => Yii::t('user', 'Department'),
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }

    public function afterDelete()
    {
        $this->profile->delete();
        Yii::$app->authManager->revokeAll($this->id);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function getDisplayName()
    {
        return $this->username;
    }

    public function getName()
    {
        return $this->profile->first_name . ' ' . $this->profile->last_name;
    }

    public function getFullName()
    {
        return $this->profile->first_name . ' ' . $this->profile->last_name . ' ' . $this->profile->middle_name;
    }

    public function getFirstName()
    {
        return $this->profile->first_name;
    }

    public function getLastName()
    {
        return $this->profile->last_name;
    }

    public function getMiddleName()
    {
        return $this->profile->middle_name;
    }

    public function getProfile() {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }

    public function getRoles() {
        return Yii::$app->authManager->getRolesByUser($this->id);
    }

    public function getInstitute(){
        return $this->hasOne(Institute::className(), ['id' => $this->profile->institute_id]);
    }

    public function getRolesAsArray() {
        $result = [];
        foreach($this->roles as $role) {
            $result[] = $role->name;
        }
        return $result;
    }

    public function getRolesAsString() {
        $result = '';
        foreach ($this->roles as $role) {
            $result .= empty($result) ? $role->name : ', ' . $role->name;
        }
        return $result;
    }

    public function getStatusAsString() {
        $statuses = $this->statusDropdown();
        return $statuses[$this->status];
    }

    public function getInstituteAsString() {
        return $this->profile->institute->name;
    }

    public function getDepartmentAsString() {
        return $this->profile->department->name;
    }

    public function getCreated() {
        return $this->created_at;
    }

    public function getUpdated() {
        return $this->updated_at;
    }

    public static function getUsersWithPermission($permission) {
        return self::find()
            ->from(self::tableName() . ' as u')
            ->leftJoin('auth_assignment as aa', 'aa.user_id = u.id')
            ->leftJoin('auth_item_child as aic', 'aic.parent = aa.item_name')
            ->where("aic.child = '$permission'")
            ->with('profile')
            ->all();
    }

    public static function statusDropdown() {
        $dropdown = [];
        $constPrefix = "STATUS_";

        // create a reflection class to get constants
        $reflClass = new ReflectionClass(get_called_class());
        $constants = $reflClass->getConstants();

        // check for status constants (e.g., STATUS_ACTIVE)
        foreach ($constants as $constantName => $constantValue) {

            // add prettified name to dropdown
            if (strpos($constantName, $constPrefix) === 0) {
                $prettyName = str_replace($constPrefix, "", $constantName);
                $prettyName = Inflector::humanize(strtolower($prettyName));
                $dropdown[$constantValue] = Yii::t('user', $prettyName);
            }
        }

        return $dropdown;
    }

    public function generateAccessToken() {
        $token = hash('sha512', time() . rand());
        $this->access_token = $token;
        $this->save();
        return $token;
    }

    public function disableAccessToken() {
        $this->access_token = null;
        $this->save();
    }
}