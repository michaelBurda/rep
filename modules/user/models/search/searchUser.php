<?php

namespace app\modules\user\models\search;

use app\modules\department\models\Department;
use app\modules\institute\models\Institute;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\models\User;
use app\modules\user\models\Profile;

/**
 * search represents the model behind the search form about `app\modules\user\models\User`.
 */
class searchUser extends User
{
    public $employmentStatus;
    public $firstName;
    public $lastName;
    public $middleName;
    public $fullName;
    public $rolesAsString;
    public $instituteAsString;
    public $departmentAsString;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'profile_id'], 'integer'],
            [['email', 'username', 'password', 'auth_key', 'access_token', 'fullName', 'firstName', 'lastName', 'middleName', 'instituteAsString', 'departmentAsString', 'employmentStatus', 'rolesAsString'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $userModule = \Yii::$app->getModule('user');
        $tn_user = User::tableName();
        $tn_profile = Profile::tableName();
        $tn_institute = Institute::tableName();
        $tn_department = Department::tableName();

//        $query = User::find()
//            ->from("$tn_user as u");
        $query = User::find()
            ->from("$tn_user as u")
            ->leftJoin("$tn_profile as p", 'p.id = u.profile_id')
            ->leftJoin("$tn_institute as i", 'i.id = p.institute_id')
            ->leftJoin("$tn_department as d", 'd.id = p.department_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $userModule->userPerPage,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'firstName' => [
                  'asc' => ['first_name' => SORT_ASC],
                  'desc' => ['first_name' => SORT_DESC],
                  'label' => Yii::t('user', 'First name'),
                  'default' => SORT_ASC
                ],
                'lastName' => [
                  'asc' => ['last_name' => SORT_ASC],
                  'desc' => ['last_name' => SORT_DESC],
                  'label' => Yii::t('user', 'Last name'),
                  'default' => SORT_ASC
                ],
                'middleName' => [
                  'asc' => ['middle_name' => SORT_ASC],
                  'desc' => ['middle_name' => SORT_DESC],
                  'label' => Yii::t('user', 'Middle name'),
                  'default' => SORT_ASC
                ],
                'fullName' => [
                    'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC, 'middle_name' => SORT_ASC],
                    'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC, 'middle_name' => SORT_DESC],
                    'label' => Yii::t('user', 'Name'),
                ],
                'instituteAsString' => [
                    'asc' => ['i.name' => SORT_ASC],
                    'desc' => ['i.name' => SORT_DESC],
                    'label' => Yii::t('user', 'Institute'),
                ],
                'departmentAsString' => [
                    'asc' => ['d.name' => SORT_ASC],
                    'desc' => ['d.name' => SORT_DESC],
                    'label' => Yii::t('user', 'Department'),
                ]
            ]
        ]);


        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

//        $query->leftJoin("$tn_profile as p", 'p.id = u.profile_id');

        if (!empty($this->firstName)) {
            $query->andWhere('p.first_name LIKE "%' . $this->firstName . '%"');
        }
        if (!empty($this->lastName)) {
            $query->andWhere('p.last_name LIKE "%' . $this->lastName . '%"');
        }
        if (!empty($this->middleName)) {
            $query->andWhere('p.middle_name LIKE "%' . $this->middleName . '%"');
        }

        if (!empty($this->fullName)) {
            $query->andWhere('p.first_name LIKE "%' . $this->fullName . '%" ' .
                'OR p.last_name LIKE "%' . $this->fullName . '%"' .
                'OR p.middle_name LIKE "%' . $this->fullName . '%"'
            );
        }

        if(!empty($this->instituteAsString)){
            $query->andWhere('i.name LIKE "%' . $this->instituteAsString . '%"');
        }

        if(!empty($this->departmentAsString)){
            $query->andWhere('d.name LIKE "%' . $this->departmentAsString . '%"');
        }

        if (!empty($this->employmentStatus)) {
            $query->andWhere(['p.status' => $this->employmentStatus]);
        }

        if (!empty($this->rolesAsString)) {
            $roles_type = 1;
            $query->leftJoin('auth_assignment as aa', 'aa.user_id = u.id')
                ->leftJoin('auth_item as ai', 'ai.name = aa.item_name')
                ->andWhere(['ai.name' => $this->rolesAsString])
                ->andWhere(['ai.type' => $roles_type]);
        }

        return $dataProvider;
    }

    // check, is string contain comparable symbols ('>=', '<=', '>', '<')
    // and return false, or ['condition' => '...', 'value' => '...']
    private function isComparable($string) {
        $conditions = ['>=', '<=', '>', '<'];
        foreach ($conditions as $condition) {
            if (strpos($string, $condition) === 0) {
                return [
                    'condition' => $condition,
                    'value' => str_replace($condition, '', $string)
                ];
            }
        }
        return false;
    }
}
