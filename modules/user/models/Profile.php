<?php
namespace app\modules\user\models;

use yii;
use yii\db\ActiveRecord;
use app\modules\institute\models\Institute;
use app\modules\department\models\Department;
/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property User $user
 */
class Profile extends ActiveRecord
{
    public static function tableName()
    {
        return 'profile';
    }

    public function rules()
    {
        return [
            [['first_name', 'last_name', 'middle_name'], 'string', 'max' => 255],
            [['institute_id', 'department_id'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'Id'),
            'first_name' => Yii::t('user', 'First name'),
            'last_name' => Yii::t('user', 'Last name'),
            'middle_name' => Yii::t('user', 'Middle name'),
            'institute_id' => Yii::t('user', 'Institute'),
            'department_id' => Yii::t('user', 'Department'),
            'instituteAsString' => Yii::t('user', 'Institute'),
            'departmentAsString' => Yii::t('user', 'Department'),
        ];
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['profile_id' => 'id']);
    }

    public function getInstitutes() {
        return Institute::find()->all();
    }

    public function getInstitute(){
        return $this->hasOne(Institute::className(), ['id' => 'institute_id']);
    }

    public function getInstitutesAsArray() {
        $result = [];
        foreach($this->institutes as $institute) {
            $result[] = $institute->name;
        }
        return $result;
    }

    public function getInstituteAsString() {
        //$institutes = $this->institutes;
        //return $this->institutes[$this->institute_id - 1]->name;
        return $this->institute ? $this->institute->name : null;
    }

    public function getDepartments() {
        return Department::find()->all();
    }

    public function getDepartment(){
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    public function getDepartmentsAsArray() {
        $result = [];
        foreach($this->departments as $department) {
            $result[] = $department->name;
        }
        return $result;
    }

    public function getDepartmentAsString() {
        //$institutes = $this->institutes;
        //return $this->departments[$this->department_id - 1]->name;
        return $this->department ? $this->department->name : null;
    }
}