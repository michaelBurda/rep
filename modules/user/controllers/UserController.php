<?php

namespace app\modules\user\controllers;

use app\modules\department\models\Department;
use app\modules\institute\models\Institute;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\rbac\Role;
use app\modules\user\models\User;
use app\modules\user\models\Profile;
use app\modules\user\models\search\searchUser;
use yii\web\ForbiddenHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['welcome'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['welcome'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionWelcome()
    {
        $user = Yii::$app->user->getIdentity();
        $profile = $user->profile;
        $roles = $user->roles;
        $userModel = $this->findModel($profile->id);
        
        return $this->render('welcome', array(
            'user' => $user,
            'profile' => $profile,
            'roles' => $roles,
            'userModel' => $userModel,
        ));
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionList()
    {
        if (!Yii::$app->user->can('user.read')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
        $searchModel = new searchUser();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $actionButtonsTemplate = Yii::$app->actionButtonsHelper->getActionButtonsTemplate('user');

        $roles = Yii::$app->authManager->roles;

        $otherUsers = User::find()
            ->with('profile')
            ->where(['!=', 'id', Yii::$app->user->id])
            ->all();

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actionButtonsTemplate' => $actionButtonsTemplate,
            'roles' => $roles,
            'otherUsers' => $otherUsers,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('user.read') && !Yii::$app->user->can('user.profileView')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
        return $this->render('view', [
            'userModel' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('user.create')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
        $userModel = new User();
        $profileModel = new Profile();
        $roles = Yii::$app->authManager->roles;
        $statuses = User::statusDropdown();
        $institutes = Institute::find()->all();
        $departments = Department::find()->all();

        if ($userModel->load(Yii::$app->request->post()) && $profileModel->load(Yii::$app->request->post())) {
            $isValid = $userModel->validate();
            $isValid = $profileModel->validate() && $isValid;
            if ($isValid) {

                // save profile and user
                $profileModel->institute_id = $profileModel->department->institute->id;
                $profileModel->save(false);
                $userModel->profile_id = $profileModel->id;
                $userModel->password = Yii::$app->security->generatePasswordHash($userModel->password);

                $userModel->created_at = date('Y-m-d h:i:s');
                $userModel->updated_at = date('Y-m-d h:i:s');
                $userModel->save(false);

                // save user roles
                if (!empty(Yii::$app->request->post('User')['rolesAsArray'])) {
                    foreach(Yii::$app->request->post('User')['rolesAsArray'] as $key => $roleName) {
                        $role = new Role();
                        $role->name = $roleName;
                        Yii::$app->authManager->assign($role, $userModel->id);
                    }
                }

                return $this->redirect(['view', 'id' => $userModel->id]);
            }
        }
        return $this->render('create', [
            'userModel' => $userModel,
            'profileModel' => $profileModel,
            'roles' => $roles,
            'statuses' => $statuses,
            'institutes' => $institutes,
            'departments' => $departments,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('user.update') && !Yii::$app->user->can('user.profileView')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
        $userModel = $this->findModel($id);
        $profileModel = $userModel->profile;
        $roles = Yii::$app->authManager->roles;
        $statuses = User::statusDropdown();
        $institutes = Institute::find()->all();
        $departments = Department::find()->all();

        if ($userModel->load(Yii::$app->request->post()) && $profileModel->load(Yii::$app->request->post())) {
            $isValid = $userModel->validate();
            $isValid = $profileModel->validate() && $isValid;

            if ($isValid) {

                // save profile and user
                $profileModel->save(false);
                if (Yii::$app->user->can('user.create')) {
                    if (!empty($userModel->password)) {
                        $userModel->password = Yii::$app->security->generatePasswordHash($userModel->password);
                    } else {
                        $userModel->password = $userModel->oldAttributes['password'];
                    }
                }
                $userModel->updated_at = date('Y-m-d h:i:s');
                $userModel->save(false);

                // save user roles
                foreach($roles as $role){
                    // remove all roles from user
                    if (Yii::$app->user->can('user.create')) Yii::$app->authManager->revoke($role, $userModel->id);
                }
                if (!empty(Yii::$app->request->post('User')['rolesAsArray'])) {
                    foreach(Yii::$app->request->post('User')['rolesAsArray'] as $key => $roleName) {
                        $role = new Role();
                        $role->name = $roleName;
                        Yii::$app->authManager->assign($role, $userModel->id);
                    }
                }

                return $this->redirect(['view', 'id' => $userModel->id]);
            }
        }
        return $this->render('update', [
            'userModel' => $userModel,
            'profileModel' => $profileModel,
            'roles' => $roles,
            'statuses' => $statuses,
            'institutes' => $institutes,
            'departments' => $departments,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('user.delete')) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
        $model = $this->findModel($id);
        if(!empty($model->profile_image)) {
            @unlink('uploads/avatars/' . $model->profile_image);
        }
        $model->delete();
        return $this->redirect(['list']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
