$(document).on('click', 'body', function(e){
  if (!$(e.target).closest('.user-more-info').length) {
    $('#user_more_info_container').animate({right:-$('#user_more_info_container').outerWidth(true)}, 500);
  }
});

$(document).on('click', '.user-more-info', function(e){
  var id = $(this).parent().attr('data-key');
  $.ajax({
    url: "/employee/info/" + id,
    method: 'get',
    success: function (userData) {
      $('#user_more_info_container').html('<div style="position: absolute; top: 50%; left: 50%;"><img src="/images/loading.svg"></div>');
      setTimeout(function() {
        $('#user_more_info_container').html(
          "<span class='glyphicon glyphicon-arrow-right close' aria-hidden='true'></span>" +
          "<div class='profile-image text-center'>" + "<img class='user-more-info-image image-responsive' src='" + userData.profile_image.value + "' width=125 height=125></div>" +
          "<div class='container-fluid'>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.first_name.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.first_name.value + "." + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.last_name.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.last_name.value + "." + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.middle_name.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.middle_name.value + "." + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.roles.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.roles.value + "." + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.email.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.email.value + "." + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='col-xs-5 col-xs-offset-1'>" + userData.updated.label + ": " + "</div>" +
          "<div class='col-xs-6'>" + userData.updated.value + "." + "</div>" +
          "</div>" +  "<br>" +
          "</div>"
        );
      }, 1000);
      if (parseInt($('#user_more_info_container').css("right")) < -100) {
        $('#user_more_info_container').animate({right: -$('#user_more_info_container').outerWidth(true)}, 500).animate({right: 0}, 500);
      }
    },
    error: function() {
    },
    complete: function() {
    }
  })

});

$(document).on('click', '#user_more_info_container span.close', function(e){
  $('#user_more_info_container').animate({right:-$('#user_more_info_container').outerWidth(true)}, 500);
});

