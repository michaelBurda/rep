<?php

namespace app\modules\rbac;
use app\modules\report\models\Report;
use Yii;
use yii\rbac\Rule;
use yii\rbac\Item;


class ReportManageRule extends Rule
{
    public $name = 'isReportCreator';
    /**
     * @param string|integer $user   the user ID.
     * @param Item           $item   the role or permission that this rule is associated with
     * @param array          $params parameters passed to ManagerInterface::checkAccess().
     *
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (isset(Yii::$app->request->queryParams['id'])) {
            $report = Report::findOne(Yii::$app->request->queryParams['id']);
            return (isset($report) and Yii::$app->user->id == $report->author_id) ? true : false;
        }
        return false;
    }
}
?>