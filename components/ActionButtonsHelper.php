<?php

namespace app\components;

use Yii;
use yii\base\Component;

class ActionButtonsHelper extends Component
{
    // return actions buttons template for current user (depends on permissions)
    public function getActionButtonsTemplate($entity, $withCrud = true, $additionalPermissions = null) {
        $permissions = [];
        if ($withCrud) {
            $permissions = [
                'read' => 'view',
                'update' => 'update',
                'delete' => 'delete',
            ];
        }

        // user->can don't used for decrease db queries count
        $userPermissions = Yii::$app->authManager->getPermissionsByUser(Yii::$app->user->id);
        $result = '';
        foreach ($permissions as $permission => $template) {
            if (array_key_exists("$entity.$permission", $userPermissions)) {
                $result .= ' {' . $template . '}';
            }
        }

        if ($additionalPermissions !== null) {
            foreach ($additionalPermissions as $permission => $template) {
                if (array_key_exists($permission, $userPermissions)) {
                    $result .= ' {' . $template . '}';
                }
            }
        }

        return $result;
    }
}
