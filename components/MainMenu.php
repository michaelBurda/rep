<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\Url;

class MainMenu extends Component
{
    // menu item properties:
    // - url
    // - label
    // - icon
    // - items (array)
    // - permissions (not required) - user must have any of seted permissions
    // - activeRule - array of urls ('/<action>') or urls templates ('/<action>/*'), which set, when menu item will be active
    // - callback - function will be called for current menu item
    // - counter
    // - counterId - html id attribute for counter
    // - options[] - array of html attributes
    private $_menu = [
        [
            'url' => '/welcome',
            'label' => 'Home',
            'icon' => 'home',
        ],
        [
            'url' => '/user/list',
            'label' => 'Users',
            'icon' => 'user',
            'permissions' => ['user.all'],
            'activeRule' => ['/user*']
        ],
        [
            'url' => '/institute/list',
            'label' => 'Institutes',
            'icon' => 'education',
            'permissions' => ['institute.read'],
            'activeRule' => ['/institute*']
        ],
        [
            'url' => '/department/list',
            'label' => 'Departments',
            'icon' => 'book',
            'permissions' => ['department.read'],
            'activeRule' => ['/department*']
        ],
        [
            'url' => '/coefficient/list',
            'label' => 'Coefficients',
            'icon' => 'scale',
            'permissions' => ['coefficient.read'],
            'activeRule' => ['/coefficient*']
        ],
        [
            'url' => '/report/list',
            'label' => 'Reports',
            'icon' => 'file',
            'permissions' => ['report.read'],
            'activeRule' => ['/report*']
        ],
        [
            'label' => 'RBAC',
            'icon' => 'sunglasses',
            'permissions' => ['/rbac/*'],
            'items' =>
            [
                [
                    'url' => '/rbac/assignment',
                    'label' => 'Assignments',
                    'activeRule' => ['/rbac/assignment*']
                ],
                [
                    'url' => '/rbac/role',
                    'label' => 'Roles',
                    'activeRule' => ['/rbac/role*']
                ],
                [
                    'url' => '/rbac/permission',
                    'label' => 'Permission',
                    'activeRule' => ['/rbac/permission*']
                ],
                [
                    'url' => '/rbac/route',
                    'label' => 'Routes',
                    'activeRule' => ['/rbac/route*']
                ],
                [
                    'url' => '/rbac/rule',
                    'label' => 'Rules',
                    'activeRule' => ['/rbac/rule*']
                ],
            ]
        ],
    ];

    private $_userPermissions;
    private $_currentUrl;
    private $_activeMenuItemSelected = false;

    public function getTitle() {
        return Yii::t('app', 'Menu');
    }

    public function getItems() {
        // private data initialization
        $this->_userPermissions = Yii::$app->authManager->getPermissionsByUser(Yii::$app->user->id);
        $this->_currentUrl = '/' . Yii::$app->request->getPathInfo();
        $this->_currentUrl = Url::current();

        $result = $this->_menu;
        $this->checkMenuItemsPermissions($result);
        return $result;
    }

    // recursive function for check menu items (and sub menu items) permissions
    // if user don't have required permissions, menu item property 'visible' = false
    private function checkMenuItemsPermissions(&$menuItems){
        foreach($menuItems as &$menuItem) {

            // call callback function, if exist
            if (isset($menuItem['callback']) && method_exists($this, $menuItem['callback'])) {
                $this->$menuItem['callback']($menuItem);
            }

            // check, is menu item need permissions, and check is user have that permissions
            // user->can don't used for decrease db queries count
            if (isset($menuItem['permissions'])) {
                $haveRequiredPermission = false;
                foreach ($menuItem['permissions'] as $permission) {
                    if (array_key_exists($permission, $this->_userPermissions)) {
                        $haveRequiredPermission = true;
                        break;
                    }
                }
                $menuItem['visible'] = $haveRequiredPermission;
            }

            // set active menu item, if not selected
            if (!$this->_activeMenuItemSelected) {
                if (!isset($menuItem['activeRule'])) {
                    // menu item don't have active rule, item will be selected if current url = menu item url
                    if (isset($menuItem['url']) && $menuItem['url'] == $this->_currentUrl) {
                        $menuItem['active'] = true;
                        $this->_activeMenuItemSelected = true;
                    }
                } else {

                    foreach ($menuItem['activeRule'] as $rule) {
                        if (strpos($rule, '*') > 0) {
                            // active rule format is: '/<action>/*'
                            $rule = substr($rule, 0, -1);
                            // menu item have active rule; check is current url start with rule string
                            if (strpos($this->_currentUrl, $rule) === 0) {
                                $menuItem['active'] = true;
                                $this->_activeMenuItemSelected = true;
                                break;
                            }
                        } else {
                            // active rule format is simple url: '/<action>
                            if ($this->_currentUrl == $rule) {
                                $menuItem['active'] = true;
                                $this->_activeMenuItemSelected = true;
                                break;
                            }
                        }
                    }
                }
            }

            // menu localization
            $menuItem['label'] = Yii::t('app', $menuItem['label']);

            if (isset($menuItem['counter'])) {
                if (isset($menuItem['counterId'])) {
                    $counterTemplate = '<span class="pull-right badge" id="' . $menuItem['counterId'] . '">'. $menuItem['counter'] . '</span> ';
                } else {
                    $counterTemplate = '<span class="pull-right badge">'. $menuItem['counter'] . '</span> ';
                }
                $menuItem['label'] = $counterTemplate . $menuItem['label'];
            }

            if (isset($menuItem['items'])) {
                // menu item have sub menu, check all sub menu items
                $this->checkMenuItemsPermissions($menuItem['items']);
            }
        }
        unset($menuItem);
    }
}
