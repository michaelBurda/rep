var userStatusTooltip = $('.user-status').tooltipster({
  content: 'Loading...',
  updateAnimation: false,
  contentAsHTML: true,
  interactive: true,
  trigger: 'click',
  functionBefore: function(origin, continueTooltip) {
    continueTooltip();
    if (origin.data('ajax') !== 'cached') {
      $.get($(this).attr('data-url'),
        function(dataGet) {
          var content = '';
          if(!dataGet) {
            content = 'Woops - there was an error.';
            origin.tooltipster('content', content);
          } else {
            content = $(dataGet);
            origin.tooltipster('content', content).data('ajax', 'cached');
            /* Ajax submit form */
            $(document).on('beforeSubmit', '#userStatusForm', function(event) {
              var form = $(this);
              $.ajax({
                url: form.attr('action'),
                method: form.attr('method'),
                data: form.serialize(),
                success: function (dataForm) {
                  if (dataForm.length) {
                    userStatusTooltip.tooltipster('hide');
                    origin.find('span').text(dataForm);
                  }
                },
                error: function () {
                  alert('error');
                },
                complete: function() {
                }
              });
              return false;
            });
            /* end submit form */
          }
        });
      origin.data('ajax', 'cached');
    }
  },
});