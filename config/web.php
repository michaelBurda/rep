<?php

$params = require(__DIR__ . '/params.php');
$config = [
    'id' => 'basic',
    'language' => 'uk-UA',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'languagepicker'],
    'aliases' => [
        '@mdm/admin' => '@app/modules/rbac',
    ],
    'timeZone' => 'UTC+2',
    'as access' => [
        'class' => 'app\modules\rbac\components\AccessControl',
        'allowActions' => [
            'user/*',
            'site/*',
            'gii/*',
            'debug/*',
            'institute/*',
            'department/*',
            'coefficient/*',
            'report/*'
//            'rbac/*',
        ]
    ],
    'components' => [
        'request' => [
            'baseUrl' => '',
            'cookieValidationKey' => 'Bd8hiAJQf02x',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',

                // user module url settings
                'login' => 'user/auth/login',
                'logout' => 'user/auth/logout',
                'welcome' => 'user/user/welcome',
                'user/login' => 'user/auth/login',
                'user/logout' => 'user/auth/logout',
                'user' => 'user/user/welcome',
                'user/<action:\w+>' => 'user/user/<action>',
                'user/<action:\w+>/<id:\w+>' => 'user/user/<action>',


                'employee' => 'user/employee/list',
                'employee/<action:\w+>' => 'user/employee/<action>',
                'employee/<action:\w+>/<id:\w+>' => 'user/employee/<action>',

                // rbac module url settings
                'rbac' => 'rbac/assignment',

                // institute module url settings
                'institute' => 'institute/institute/list',
                'institute/<action:\w+>/<id:\w+>' => 'institute/institute/<action>',
                'institute/<action:\w+>' => 'institute/institute/<action>',

                // department module url settings
                'department' => 'department/department/list',
                'department/<action:\w+>/<id:\w+>' => 'department/department/<action>',
                'department/<action:\w+>' => 'department/department/<action>',

                // coefficient module url settings
                'coefficient' => 'coefficient/coefficient/list',
                'coefficient/<action:\w+>/<id:\w+>' => 'coefficient/coefficient/<action>',
                'coefficient/<action:\w+>' => 'coefficient/coefficient/<action>',

                // report module url settings
                'report' => 'report/report/list',
                'report/<action:\w+>/<id:\w+>' => 'report/report/<action>',
                'report/<action:\w+>' => 'report/report/<action>',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'app\modules\user\components\User',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'on missingTranslation' => ['app\components\TranslationEventHandler', 'handleMissingTranslation'],
                ],
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'username',
                'password' => 'password',
                'port' => '587',
                'encryption' => 'tls',
            ],
            'useFileTransport' => true,
            'messageConfig' => [
                'from' => ['admin@supervisor.com' => 'Admin'],
                'charset' => 'UTF-8',
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'mainMenu' => [
            'class' => 'app\components\MainMenu',
        ],
        'actionButtonsHelper' => [
            'class' => 'app\components\ActionButtonsHelper',
        ],
        'languagepicker' => [
            'class' => 'lajax\languagepicker\Component',
            'languages' => ['uk-UA' => 'Українська', 'en-US' => 'English(US)'],
            'cookieName' => 'language',                         
            'expireDays' => 64,                                                          
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'forceCopy' => true,
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'app\modules\user\Module',
        ],
        'rbac' => [
            'class' => 'app\modules\rbac\Module',
        ],
        'institute' => [
            'class' => 'app\modules\institute\Module',
        ],
        'department' => [
            'class' => 'app\modules\department\Module',
        ],
        'coefficient' => [
            'class' => 'app\modules\coefficient\Module',
        ],
        'report' => [
            'class' => 'app\modules\report\Module',
        ],
    ],
    'params' => $params,
];

$config = \yii\helpers\ArrayHelper::merge($config, require(__DIR__ . '/db.php'));

//if (YII_ENV_DEV) {
//    // configuration adjustments for 'dev' environment
//    $config['bootstrap'][] = 'debug';
//    $config['modules']['debug'] = [
//        'class' => 'yii\debug\Module',
//    ];
//
//    $config['bootstrap'][] = 'gii';
//    $config['modules']['gii'] = [
//        'class' => 'yii\gii\Module',
//        'allowedIPs' => ['*.*.*.*'],
//    ];
//}
return $config;
