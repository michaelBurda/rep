<?php

return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=reports-db',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
    ]
];
